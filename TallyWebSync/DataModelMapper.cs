﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TallyWebSync
{
    public enum LedgerGroup { [Description("Sundry Debtors")]SD};
   public static class DataModelMapper
    {
        public static string GetDescription<T>(this T e) where T : IConvertible
        {
            if (e is Enum)
            {
                Type type = e.GetType();
                Array values = System.Enum.GetValues(type);

                foreach (int val in values)
                {
                    if (val == e.ToInt32(CultureInfo.InvariantCulture))
                    {
                        var memInfo = type.GetMember(type.GetEnumName(val));
                        var descriptionAttribute = memInfo[0]
                            .GetCustomAttributes(typeof(DescriptionAttribute), false)
                            .FirstOrDefault() as DescriptionAttribute;

                        if (descriptionAttribute != null)
                        {
                            return descriptionAttribute.Description;
                        }
                    }
                }
            }

            return null; // could also return string.Empty
        }


        public static List<CustomreList> LedgerToCustomers(List<ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGER> listLEDGER)
        {
            //chech null
            //add only data having sudary 

            List<CustomreList> customreLists = new List<CustomreList>();
            foreach (var ledger in listLEDGER)
            {
                var ledgergroup = LedgerGroup.SD.GetDescription().ToString();
                if (ledgergroup == ledger.PARENT || ledger.PARENT.ToLower()=="customers")
                {
                    customre-Lists.Add(LedgerToCustomer(ledger));
                }


            }
            return customreLists;




        }
        public static CustomreList LedgerToCustomer(ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGER ledger)
        { 
            string lname = "";
            //if (ledger.LANGUAGENAMELIST.NAMELIST.NAME. != null)
            //{
            //    lname = ledger.LANGUAGENAMELIST.NAMELIST.NAME[1].ToString();
            //}
            //for (int i = 0; ledger.LANGUAGENAMELIST.NAMELIST.NAME < i; i++)
            //{
            //}
             
            CustomreList customre = new CustomreList()
            {
                fname = ledger.NAME,
               // lname = lname,
                lname = ledger.LANGUAGENAMELIST.NAMELIST.NAME.ToList().Skip(1).FirstOrDefault(),
                contact_name = ledger.LEDGERCONTACT.ToString(),
                //customer_code=ledger.LANGUAGENAMELIST.NAMELIST.NAME[2].ToString(),
                address=ledger.ADDRESSLIST!=null?ledger.ADDRESSLIST.ADDRESS.FirstOrDefault():"",
                mobile=ledger.LEDGERMOBILE.ToString(),
                phone=ledger.LEDGERPHONE.ToString(),
                email = ledger.EMAIL.ToString(),
                gstin = ledger.GSTNO.ToString(),
                remarks = ledger.PARENT.ToString(),
                type = ledger.RESERVEDNAME.ToString(),
                status="1",
                created_by=ledger.CREATEDBY.ToString(),
                created_on=ledger.CREATEDDATE.ToString()                
            };
                



            return customre;
        }

    }
}
