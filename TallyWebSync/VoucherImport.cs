﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TallyWebSync.Modal;

namespace TallyWebSync
{
    class VoucherImport
    {
        public async Task Getvocher()
        {
            
            XmlFormte xmlfromateXmlFormte = new XmlFormte();
            var XmlvoucherImportRequest = xmlfromateXmlFormte.XmlVoucherImport();
            var data = "";
            string xml_da;
            var httpClient = new HttpClient();
            var httpContent = new StringContent(XmlvoucherImportRequest);
            var requestUri = "http://localhost:9000";
            var response = await httpClient.PostAsync(requestUri, httpContent);
            using (HttpContent content = response.Content)
            {
                data = await content.ReadAsStringAsync();
            }
        }
        public async Task PullVoucherTaskLists(List<TransactionList> transactionLists)
        {
            var responseResult = "";
            var httpClient = new HttpClient();
            var httpContent = new StringContent(JsonConvert.SerializeObject(transactionLists));
            var requestUri = "http://serv.vprotectindia.com/api/customer";


            try
            {

                var response = await httpClient.PostAsync(requestUri, httpContent);
                using (HttpContent content = response.Content)
                {
                    responseResult = await content.ReadAsStringAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}