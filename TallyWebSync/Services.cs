﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using System.Web.Mvc;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using TallyWebSync.Modal;
using System.Configuration;

namespace TallyWebSync
{
    public class Services
    {
        public string ReponseStr;
//        public ServerHttp = new MSXML2.ServerXMLHTTP30();
//Public responsstr As String
//Public ServerHTTP As New MSXML2.ServerXMLHTTP30
//Public XMLDOM As New MSXML2.DOMDocument30
//Public CHILDNODE As Object
//Public I As Integer
//Public ERRORSTR As String

        public async Task<List<Customer>> GetCustomers(string requestType)
        {
            var customers = new List<Customer>();
            XmlFormte xmlfromateXmlFormte = new XmlFormte();
            var XmlLedgerExportRequest = XmlData.XmlLedgerRequest;//XmlData.XmlLedgerRequestSaleReg; //
            var data ="";
            string xml_da;
            var httpClient = new HttpClient();
            var httpContent = new StringContent(XmlLedgerExportRequest);
            var requestUri = "http://localhost:9000";
           
            
            try
            {
             
                var response = await httpClient.PostAsync(requestUri, httpContent);
                using (HttpContent content = response.Content)
                {
                     data = await content.ReadAsStringAsync();
                }

              XmlDocument doc = new XmlDocument();
               
                data = data.Replace(@"#", "");
                data = data.Replace(@"&", "");
               var xml_data= XDocument.Parse(data.ToString());
                using (StringWriter writer = new StringWriter())
                {
                    xml_data.Save(writer);
                    xml_da = writer.ToString();
                     
                }



               
                var ledgers= DeserializeTallyEnvelope(xml_da).BODY.IMPORTDATA.REQUESTDATA.Where(x=>x.LEDGER!=null).Select(x=>x.LEDGER).ToList();

                var customerLists = DataModelMapper.LedgerToCustomers(ledgers);
               await PostCustomerLists(customerLists);


                if (response.IsSuccessStatusCode)
                {
                    throw new Exception(string.Format("Invalid uri: {0}", XmlData.XmlLedgerExportRequest));
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            


            return customers;

        }
        public async Task PostCustomerLists( List<CustomreList> customreLists)
        {
            var responseResult ="";
            var httpClient = new HttpClient();
            var httpContent = new StringContent(JsonConvert.SerializeObject(customreLists));
           // var requestUri = "https://crm.vprotectindia.com/api/cust_master_api.php";

            string requestUri = ConfigurationManager.AppSettings["customer_master"].ToString();


            try
            {
                var response = await httpClient.PostAsync(requestUri, httpContent);
                using (HttpContent content = response.Content)
                {
                    responseResult = await content.ReadAsStringAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        public async Task<List<SaleSubsList>> Getsubs(string requestType)
        {
            var salesubs = new List<SaleSubsList>();
            XmlFormte xmlfromateXmlFormte = new XmlFormte();
            var XmlLedgerExportRequest = XmlData.XmlLedgerRequest;//XmlData.XmlLedgerRequestSaleReg; //
            var data = "";
            string xml_da;
            var httpClient = new HttpClient();
            var httpContent = new StringContent(XmlLedgerExportRequest);
            var requestUri = "http://localhost:9000";


            try
            {

                var response = await httpClient.PostAsync(requestUri, httpContent);
                using (HttpContent content = response.Content)
                {
                    data = await content.ReadAsStringAsync();
                }

                XmlDocument doc = new XmlDocument();

                data = data.Replace(@"#", "");
                data = data.Replace(@"&", "");
                var xml_data = XDocument.Parse(data.ToString());
                using (StringWriter writer = new StringWriter())
                {
                    xml_data.Save(writer);
                    xml_da = writer.ToString();

                } 

                var ledgers = DeserializeTallyEnvelope(xml_da).BODY.IMPORTDATA.REQUESTDATA.Where(x => x.LEDGER != null).Select(x => x.LEDGER).ToList();

                var salesubsLists = DataModelMapper.LedgerToCustomers(ledgers);
                await PostsubsLists(salesubsLists);



                if (response.IsSuccessStatusCode)
                {
                    throw new Exception(string.Format("Invalid uri: {0}", XmlData.XmlLedgerExportRequest));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            } 

            return salesubs;

        }
        public async Task PostsubsLists(List<CustomreList> subsLists)
        {
            var responseResult = "";
            var httpClient = new HttpClient();
            var httpContent = new StringContent(JsonConvert.SerializeObject(subsLists));
            // var requestUri = "https://crm.vprotectindia.com/api/cust_master_api.php";

            string requestUri = ConfigurationManager.AppSettings["customer_master"].ToString();


            try
            {

                var response = await httpClient.PostAsync(requestUri, httpContent);
                using (HttpContent content = response.Content)
                {
                    responseResult = await content.ReadAsStringAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }



       
        public ENVELOPE DeserializeTallyEnvelope(string tallyEnvXml)
        {
            ENVELOPE tallyEnvelop;
            using (TextReader sr = new StringReader(tallyEnvXml))
            {
                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(ENVELOPE));
                tallyEnvelop = (ENVELOPE)serializer.Deserialize(sr);

            }

            return tallyEnvelop;
        }

    }

    public static class XmlData
    {
        public static string XmlRequestExportString =
             @"<ENVELOPE> 
                <HEADER>
                <VERSION>1</VERSION>
                <TALLYREQUEST>Export</TALLYREQUEST>
                <TYPE>Collection</TYPE>
                </HEADER>
                <BODY> 
        <DESC> 
        </DESC>
        <DATA>
        <TALLYMESSAGE >
<STARTINGFROM/>
      <STREGDATE/>
      <SAMPLINGDATEONEFACTOR/>
      <SAMPLINGDATETWOFACTOR/>
      <ACTIVEFROM/>
      <ACTIVETO/>
      <CREATEDDATE/>
      <ALTEREDON/>
      <EXCISEREGISTRATIONDATE/>
      <CURRENCYNAME>?</CURRENCYNAME>
      <EMAIL/>
      <STATENAME>Haryana</STATENAME>
      <PINCODE>122032</PINCODE>
      <INCOMETAXNUMBER/>
        </TALLYMESSAGE >
        </DATA>
        </BODY>
       </ENVELOPE>";




        public static string XmlLedgerExportRequest = @"<ENVELOPE>
 	<HEADER>
    		<VERSION>1</VERSION>
    		<TALLYREQUEST>Export</TALLYREQUEST>
    		<TYPE>Collection</TYPE>
    		<ID>Ledger</ID>
 	</HEADER>
<BODY>
<DESC>
<STATICVARIABLES>
       		    		<EXPLODEFLAG>No</EXPLODEFLAG>
       		    		<SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT>
   		  	</STATICVARIABLES>
</DESC>
</BODY>
</ENVELOPE>";

        public static string XmlLedgerRequest = @"<ENVELOPE>
<HEADER>
<TALLYREQUEST>Export Data</TALLYREQUEST>
</HEADER>
<BODY>
<EXPORTDATA>
<REQUESTDESC>
 
<STATICVARIABLES>
 <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT>

<ACCOUNTTYPE>Ledgers</ACCOUNTTYPE>
<GROUPNAME>SUNDRY DEBTORS</GROUPNAME>
<CREATEDDATE>20180401</CREATEDDATE>
<ALTERID> 90068</ALTERID>
</STATICVARIABLES>
<!--Report Name-->
<REPORTNAME>List of Accounts</REPORTNAME>
</REQUESTDESC>
</EXPORTDATA>
</BODY>
</ENVELOPE>";

        public static string XmlLedgerRequestSaleReg = @"<ENVELOPE>
    <HEADER>
    <VERSION>1</VERSION>
    <TALLYREQUEST>EXPORT</TALLYREQUEST>
    <TYPE>DATA</TYPE>
    <ID>Voucher Register</ID>
    </HEADER>
    <BODY>
    <DESC>
    <STATICVARIABLES>
    <VoucherTypeName>Sales</VoucherTypeName>
    <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT> 
    </STATICVARIABLES>
    </DESC>
    </BODY>
    </ENVELOPE>";


    }
    public class Customer
    {
        public string Name { get; set; }
        public  int CustomerId { get; set; }
    }
   
   
}
