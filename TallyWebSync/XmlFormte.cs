﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace TallyWebSync
{
    public   class  XmlFormte
    {

        public  string XmlRequestExportString()
        {

          string XmlRequestExportString =
                @"<ENVELOPE> 
                <HEADER>
                <VERSION>1</VERSION>
                <TALLYREQUEST>Export</TALLYREQUEST>
                <TYPE>Collection</TYPE>
                </HEADER>
                <BODY> 
        <DESC> 
        </DESC>
        <DATA>
        <TALLYMESSAGE >
        </TALLYMESSAGE >
        </DATA>
        </BODY>
       </ENVELOPE>";
            return XmlRequestExportString;
        }

        public  string XmlLedgerExportRequest(string xmlType)
        {

            string XmlLedgerExportRequest = @"<ENVELOPE>
 	<HEADER>
    		<VERSION>1</VERSION>
    		<TALLYREQUEST>Export</TALLYREQUEST>
    		<TYPE>Data</TYPE>
    		<ID>" + xmlType + @"</ID>
 	</HEADER>
<BODY>
<DESC>
<STATICVARIABLES>
       		    		<EXPLODEFLAG>Yes</EXPLODEFLAG>
       		    		<SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT>
   		  	</STATICVARIABLES>
</DESC>
</BODY>
</ENVELOPE>";

            return XmlLedgerExportRequest;
        }

        public string XmlVoucherImport()
        {
           string XmlVoucherImport= @"<ENVELOPE>
  <HEADER>
    <VERSION>1</VERSION>
    <TALLYREQUEST>Import</TALLYREQUEST>
    <TYPE>Data</TYPE>
    <ID>Vouchers</ID>
  </HEADER>
  <BODY>
    <DESC>
      <STATICVARIABLES>
           <IMPORTDUPS>@@DUPCOMBINE</IMPORTDUPS>
          </STATICVARIABLES>
    </DESC>
    <DATA>
      <TALLYMESSAGE>
        <VOUCHER>
          <DATE>20180401</DATE>
          <NARRATION>Ch. No. Tested</NARRATION>
          <VOUCHERTYPENAME>Payment</VOUCHERTYPENAME>
          <VOUCHERNUMBER>1</VOUCHERNUMBER>
          <ALLLEDGERENTRIES.LIST>
            <LEDGERNAME>TestSD</LEDGERNAME>
            <ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
            <AMOUNT>12000.00</AMOUNT>
          </ALLLEDGERENTRIES.LIST>
          <ALLLEDGERENTRIES.LIST>
            <LEDGERNAME>Cash</LEDGERNAME>
            <ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
            <AMOUNT>-12000.00</AMOUNT>
          </ALLLEDGERENTRIES.LIST>
        </VOUCHER>
      </TALLYMESSAGE>
       </DATA>
 </BODY>
  </ENVELOPE>";

           return XmlVoucherImport;


        }

    }
}




