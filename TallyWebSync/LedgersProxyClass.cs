﻿

namespace TallyWebSync
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ENVELOPE
    {

        private ENVELOPEHEADER hEADERField;

        private ENVELOPEBODY bODYField;

        /// <remarks/>
        public ENVELOPEHEADER HEADER
        {
            get
            {
                return this.hEADERField;
            }
            set
            {
                this.hEADERField = value;
            }
        }

        /// <remarks/>
        public ENVELOPEBODY BODY
        {
            get
            {
                return this.bODYField;
            }
            set
            {
                this.bODYField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEHEADER
    {

        private string tALLYREQUESTField;

        /// <remarks/>
        public string TALLYREQUEST
        {
            get
            {
                return this.tALLYREQUESTField;
            }
            set
            {
                this.tALLYREQUESTField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODY
    {

        private ENVELOPEBODYIMPORTDATA iMPORTDATAField;

        /// <remarks/>
        public ENVELOPEBODYIMPORTDATA IMPORTDATA
        {
            get
            {
                return this.iMPORTDATAField;
            }
            set
            {
                this.iMPORTDATAField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATA
    {

        private ENVELOPEBODYIMPORTDATAREQUESTDESC rEQUESTDESCField;

        private ENVELOPEBODYIMPORTDATATALLYMESSAGE[] rEQUESTDATAField;

        /// <remarks/>
        public ENVELOPEBODYIMPORTDATAREQUESTDESC REQUESTDESC
        {
            get
            {
                return this.rEQUESTDESCField;
            }
            set
            {
                this.rEQUESTDESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("TALLYMESSAGE", IsNullable = false)]
        public ENVELOPEBODYIMPORTDATATALLYMESSAGE[] REQUESTDATA
        {
            get
            {
                return this.rEQUESTDATAField;
            }
            set
            {
                this.rEQUESTDATAField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATAREQUESTDESC
    {

        private string rEPORTNAMEField;

        private ENVELOPEBODYIMPORTDATAREQUESTDESCSTATICVARIABLES sTATICVARIABLESField;

        /// <remarks/>
        public string REPORTNAME
        {
            get
            {
                return this.rEPORTNAMEField;
            }
            set
            {
                this.rEPORTNAMEField = value;
            }
        }

        /// <remarks/>
        public ENVELOPEBODYIMPORTDATAREQUESTDESCSTATICVARIABLES STATICVARIABLES
        {
            get
            {
                return this.sTATICVARIABLESField;
            }
            set
            {
                this.sTATICVARIABLESField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATAREQUESTDESCSTATICVARIABLES
    {

        private string sVCURRENTCOMPANYField;

        /// <remarks/>
        public string SVCURRENTCOMPANY
        {
            get
            {
                return this.sVCURRENTCOMPANYField;
            }
            set
            {
                this.sVCURRENTCOMPANYField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATATALLYMESSAGE
    {

        private ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGER lEDGERField;

        private ENVELOPEBODYIMPORTDATATALLYMESSAGEGROUP gROUPField;

        private ENVELOPEBODYIMPORTDATATALLYMESSAGECURRENCY cURRENCYField;

        /// <remarks/>
        public ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGER LEDGER
        {
            get
            {
                return this.lEDGERField;
            }
            set
            {
                this.lEDGERField = value;
            }
        }

        /// <remarks/>
        public ENVELOPEBODYIMPORTDATATALLYMESSAGEGROUP GROUP
        {
            get
            {
                return this.gROUPField;
            }
            set
            {
                this.gROUPField = value;
            }
        }

        /// <remarks/>
        public ENVELOPEBODYIMPORTDATATALLYMESSAGECURRENCY CURRENCY
        {
            get
            {
                return this.cURRENCYField;
            }
            set
            {
                this.cURRENCYField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGER
    {

        private ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGERADDRESSLIST aDDRESSLISTField;

        private ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGERMAILINGNAMELIST mAILINGNAMELISTField;

        private ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGEROLDAUDITENTRYIDSLIST oLDAUDITENTRYIDSLISTField;

        private object sTARTINGFROMField;

        private object sTREGDATEField;

        private object sAMPLINGDATEONEFACTORField;

        private object sAMPLINGDATETWOFACTORField;

        private object aCTIVEFROMField;

        private object aCTIVETOField;

        private string cREATEDDATEField;

        private object aLTEREDONField;

        private object eXCISEREGISTRATIONDATEField;

        private string cURRENCYNAMEField;

        private string eMAILField;

        private string sTATENAMEField;

        private string pINCODEField;

        private object iNCOMETAXNUMBERField;

        private object sALESTAXNUMBERField;

        private object iNTERSTATESTNUMBERField;

        private object vATTINNUMBERField;

        private object eXCISERANGEField;

        private object eXCISEDIVISIONField;

        private object eXCISECOMMISSIONERATEField;

        private object vATDEALERTYPEField;

        private object pRICELEVELField;

        private string pARENTField;

        private object sAMPLINGMETHODField;

        private object sAMPLINGSTRONEFACTORField;

        private string pARTYGSTINField;
        

        private object nARRATIONField;

        private object gRPDEBITPARENTField;

        private object gRPCREDITPARENTField;

        private object sYSDEBITPARENTField;

        private object sYSCREDITPARENTField;

        private string cREATEDBYField;

        private object aLTEREDBYField;

        private object tAXCLASSIFICATIONNAMEField;

        private string tAXTYPEField;

        private object bILLCREDITPERIODField;

        private object bANKDETAILSField;

        private object bANKBRANCHNAMEField;

        private object bANKBSRCODEField;

        private object dEDUCTEETYPEField;

        private object bUSINESSTYPEField;

        private object tYPEOFNOTIFICATIONField;

        private object mSMEREGNUMBERField;

        private object cOUNTRYOFRESIDENCEField;

        private object rELATEDPARTYIDField;

        private object rELPARTYISSUINGAUTHORITYField;

        private object iMPORTEREXPORTERCODEField;

        private object eMAILCCField;

        private string lEDGERPHONEField;

        private object lEDGERFAXField;

        private string lEDGERCONTACTField;

        private string lEDGERMOBILEField;

        private object rELATIONTYPEField;

        private object bASICTYPEOFDUTYField;

        private object gSTTYPEField;

        private object eXEMPTIONTYPEField;

        private object aPPROPRIATEFORField;

        private object sUBTAXTYPEField;

        private object sTXNATUREOFPARTYField;

        private object pAYTYPEField;

        private object pAYSLIPNAMEField;

        private object aTTENDANCETYPEField;

        private object lEAVETYPEField;

        private object cALCULATIONPERIODField;

        private object rOUNDINGMETHODField;

        private object cOMPUTATIONTYPEField;

        private object cALCULATIONTYPEField;

        private object pAYSTATTYPEField;

        private object pROFESSIONALTAXNUMBERField;

        private object uSERDEFINEDCALENDERTYPEField;

        private object iTNATUREField;

        private object iTCOMPONENTField;

        private object nOTIFICATIONNUMBERField;

        private object rEGISTRATIONNUMBERField;

        private object sERVICECATEGORYField;

        private object aBATEMENTNOTIFICATIONNOField;

        private object sTXDUTYHEADField;

        private object sTXCLASSIFICATIONField;

        private object eXCISELEDGERCLASSIFICATIONField;

        private object eXCISEREGISTRATIONNUMBERField;

        private object eXCISEACCOUNTHEADField;

        private object eXCISEDUTYTYPEField;

        private object eXCISEDUTYHEADCODEField;

        private object eXCISEALLOCTYPEField;

        private object eXCISEDUTYHEADField;

        private object nATUREOFSALESField;

        private object eXCISENOTIFICATIONNOField;

        private object eXCISEREGNOField;

        private object eXCISENATUREOFPURCHASEField;

        private object tDSDEDUCTEETYPEField;

        private object tDSRATENAMEField;

        private object tDSDEDUCTEESECTIONNUMBERField;

        private object lEDGERFBTCATEGORYField;

        private object bRANCHCODEField;

        private object iFSCODEField;

        private object cLIENTCODEField;

        private object bANKINGCONFIGBANKField;

        private object bANKINGCONFIGBANKIDField;

        private string iSBILLWISEONField;

        private string iSCOSTCENTRESONField;

        private string iSINTERESTONField;

        private string aLLOWINMOBILEField;

        private string iSCOSTTRACKINGONField;

        private string iSCONDENSEDField;

        private string aFFECTSSTOCKField;

        private string fORPAYROLLField;

        private string iSABCENABLEDField;

        private string iNTERESTONBILLWISEField;

        private string oVERRIDEINTERESTField;

        private string oVERRIDEADVINTERESTField;

        private string uSEFORVATField;

        private string iGNORETDSEXEMPTField;

        private string iSTCSAPPLICABLEField;

        private string iSTDSAPPLICABLEField;

        private string iSFBTAPPLICABLEField;

        private string iSGSTAPPLICABLEField;

        private string iSEXCISEAPPLICABLEField;

        private string iSTDSEXPENSEField;

        private string iSEDLIAPPLICABLEField;

        private string iSRELATEDPARTYField;

        private string uSEFORESIELIGIBILITYField;

        private string sHOWINPAYSLIPField;

        private string uSEFORGRATUITYField;

        private string iSTDSPROJECTEDField;

        private string fORSERVICETAXField;

        private string iSINPUTCREDITField;

        private string iSEXEMPTEDField;

        private string iSABATEMENTAPPLICABLEField;

        private string iSSTXPARTYField;

        private string iSSTXNONREALIZEDTYPEField;

        private string tDSDEDUCTEEISSPECIALRATEField;

        private string aUDITEDField;

        private byte sAMPLINGNUMONEFACTORField;

        private byte sAMPLINGNUMTWOFACTORField;

        private ushort sORTPOSITIONField;

        private byte dEFAULTLANGUAGEField;

        private byte rATEOFTAXCALCULATIONField;

        private byte gRATUITYMONTHDAYSField;

        private byte gRATUITYLIMITMONTHSField;

        private byte cALCULATIONBASISField;

        private byte rOUNDINGLIMITField;

        private byte aBATEMENTPERCENTAGEField;

        private byte tDSDEDUCTEESPECIALRATEField;

        private object sAMPLINGAMTONEFACTORField;

        private object sAMPLINGAMTTWOFACTORField;

        private string oPENINGBALANCEField;

        private object cREDITLIMITField;

        private object gRATUITYLIMITAMOUNTField;

        private object oDLIMITField;

        private object xBRLPREVYRLEDGERAMTField;

        private ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGERLANGUAGENAMELIST lANGUAGENAMELISTField;

        private object xBRLDETAILLISTField;

        private object aUDITDETAILSLISTField;

        private object sCHVIDETAILSLISTField;

        private object sLABPERIODLISTField;

        private object gRATUITYPERIODLISTField;

        private object aDDITIONALCOMPUTATIONSLISTField;

        private object bANKALLOCATIONSLISTField;

        private object pAYMENTDETAILSLISTField;

        private object bANKEXPORTFORMATSLISTField;

        private object bILLALLOCATIONSLISTField;

        private object iNTERESTCOLLECTIONLISTField;

        private object lEDGERCLOSINGVALUESLISTField;

        private object lEDGERAUDITCLASSLISTField;

        private object oLDAUDITENTRIESLISTField;

        private object tDSEXEMPTIONRULESLISTField;

        private object dEDUCTINSAMEVCHRULESLISTField;

        private object lOWERDEDUCTIONLISTField;

        private object sTXABATEMENTDETAILSLISTField;

        private object lEDMULTIADDRESSLISTLISTField;

        private object sTXTAXDETAILSLISTField;

        private object cHEQUERANGELISTField;

        private object dEFAULTVCHCHEQUEDETAILSLISTField;

        private object aCCOUNTAUDITENTRIESLISTField;

        private object aUDITENTRIESLISTField;

        private object bRSIMPORTEDINFOLISTField;

        private object aUTOBRSCONFIGSLISTField;

        private object bANKURENTRIESLISTField;

        private object dEFAULTCHEQUEDETAILSLISTField;

        private object dEFAULTOPENINGCHEQUEDETAILSLISTField;

        private string nAMEField;

        private string rESERVEDNAMEField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ADDRESS.LIST")]
        public ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGERADDRESSLIST ADDRESSLIST
        {
            get
            {
                return this.aDDRESSLISTField;
            }
            set
            {
                this.aDDRESSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("MAILINGNAME.LIST")]
        public ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGERMAILINGNAMELIST MAILINGNAMELIST
        {
            get
            {
                return this.mAILINGNAMELISTField;
            }
            set
            {
                this.mAILINGNAMELISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("OLDAUDITENTRYIDS.LIST")]
        public ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGEROLDAUDITENTRYIDSLIST OLDAUDITENTRYIDSLIST
        {
            get
            {
                return this.oLDAUDITENTRYIDSLISTField;
            }
            set
            {
                this.oLDAUDITENTRYIDSLISTField = value;
            }
        }

        /// <remarks/>
        public object STARTINGFROM
        {
            get
            {
                return this.sTARTINGFROMField;
            }
            set
            {
                this.sTARTINGFROMField = value;
            }
        }

        /// <remarks/>
        public object STREGDATE
        {
            get
            {
                return this.sTREGDATEField;
            }
            set
            {
                this.sTREGDATEField = value;
            }
        }

        /// <remarks/>
        public object SAMPLINGDATEONEFACTOR
        {
            get
            {
                return this.sAMPLINGDATEONEFACTORField;
            }
            set
            {
                this.sAMPLINGDATEONEFACTORField = value;
            }
        }

        /// <remarks/>
        public object SAMPLINGDATETWOFACTOR
        {
            get
            {
                return this.sAMPLINGDATETWOFACTORField;
            }
            set
            {
                this.sAMPLINGDATETWOFACTORField = value;
            }
        }

        /// <remarks/>
        public object ACTIVEFROM
        {
            get
            {
                return this.aCTIVEFROMField;
            }
            set
            {
                this.aCTIVEFROMField = value;
            }
        }

        /// <remarks/>
        public object ACTIVETO
        {
            get
            {
                return this.aCTIVETOField;
            }
            set
            {
                this.aCTIVETOField = value;
            }
        }

        /// <remarks/>
        public string CREATEDDATE
        {
            get
            {
                return this.cREATEDDATEField;
            }
            set
            {
                this.cREATEDDATEField = value;
            }
        }

        /// <remarks/>
        public object ALTEREDON
        {
            get
            {
                return this.aLTEREDONField;
            }
            set
            {
                this.aLTEREDONField = value;
            }
        }

        /// <remarks/>
        public object EXCISEREGISTRATIONDATE
        {
            get
            {
                return this.eXCISEREGISTRATIONDATEField;
            }
            set
            {
                this.eXCISEREGISTRATIONDATEField = value;
            }
        }

        /// <remarks/>
        public string CURRENCYNAME
        {
            get
            {
                return this.cURRENCYNAMEField;
            }
            set
            {
                this.cURRENCYNAMEField = value;
            }
        }

        /// <remarks/>
        public string EMAIL
        {
            get
            {
                return this.eMAILField;
            }
            set
            {
                this.eMAILField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("PARTYGSTIN")]
        public string GSTNO
        {
            get
            {
                return this.pARTYGSTINField;
            }
            set
            {
                this.pARTYGSTINField = value;
            }
        }


        /// <remarks/>
        public string STATENAME
        {
            get
            {
                return this.sTATENAMEField;
            }
            set
            {
                this.sTATENAMEField = value;
            }
        }

        /// <remarks/>
        public string PINCODE
        {
            get
            {
                return this.pINCODEField;
            }
            set
            {
                this.pINCODEField = value;
            }
        }

        /// <remarks/>
        public object INCOMETAXNUMBER
        {
            get
            {
                return this.iNCOMETAXNUMBERField;
            }
            set
            {
                this.iNCOMETAXNUMBERField = value;
            }
        }

        /// <remarks/>
        public object SALESTAXNUMBER
        {
            get
            {
                return this.sALESTAXNUMBERField;
            }
            set
            {
                this.sALESTAXNUMBERField = value;
            }
        }

        /// <remarks/>
        public object INTERSTATESTNUMBER
        {
            get
            {
                return this.iNTERSTATESTNUMBERField;
            }
            set
            {
                this.iNTERSTATESTNUMBERField = value;
            }
        }

        /// <remarks/>
        public object VATTINNUMBER
        {
            get
            {
                return this.vATTINNUMBERField;
            }
            set
            {
                this.vATTINNUMBERField = value;
            }
        }

        /// <remarks/>
        public object EXCISERANGE
        {
            get
            {
                return this.eXCISERANGEField;
            }
            set
            {
                this.eXCISERANGEField = value;
            }
        }

        /// <remarks/>
        public object EXCISEDIVISION
        {
            get
            {
                return this.eXCISEDIVISIONField;
            }
            set
            {
                this.eXCISEDIVISIONField = value;
            }
        }

        /// <remarks/>
        public object EXCISECOMMISSIONERATE
        {
            get
            {
                return this.eXCISECOMMISSIONERATEField;
            }
            set
            {
                this.eXCISECOMMISSIONERATEField = value;
            }
        }

        /// <remarks/>
        public object VATDEALERTYPE
        {
            get
            {
                return this.vATDEALERTYPEField;
            }
            set
            {
                this.vATDEALERTYPEField = value;
            }
        }

        /// <remarks/>
        public object PRICELEVEL
        {
            get
            {
                return this.pRICELEVELField;
            }
            set
            {
                this.pRICELEVELField = value;
            }
        }

        /// <remarks/>
        public string PARENT
        {
            get
            {
                return this.pARENTField;
            }
            set
            {
                this.pARENTField = value;
            }
        }

        /// <remarks/>
        public object SAMPLINGMETHOD
        {
            get
            {
                return this.sAMPLINGMETHODField;
            }
            set
            {
                this.sAMPLINGMETHODField = value;
            }
        }

        /// <remarks/>
        public object SAMPLINGSTRONEFACTOR
        {
            get
            {
                return this.sAMPLINGSTRONEFACTORField;
            }
            set
            {
                this.sAMPLINGSTRONEFACTORField = value;
            }
        }

        /// <remarks/>
        public object NARRATION
        {
            get
            {
                return this.nARRATIONField;
            }
            set
            {
                this.nARRATIONField = value;
            }
        }

        /// <remarks/>
        public object GRPDEBITPARENT
        {
            get
            {
                return this.gRPDEBITPARENTField;
            }
            set
            {
                this.gRPDEBITPARENTField = value;
            }
        }

        /// <remarks/>
        public object GRPCREDITPARENT
        {
            get
            {
                return this.gRPCREDITPARENTField;
            }
            set
            {
                this.gRPCREDITPARENTField = value;
            }
        }

        /// <remarks/>
        public object SYSDEBITPARENT
        {
            get
            {
                return this.sYSDEBITPARENTField;
            }
            set
            {
                this.sYSDEBITPARENTField = value;
            }
        }

        /// <remarks/>
        public object SYSCREDITPARENT
        {
            get
            {
                return this.sYSCREDITPARENTField;
            }
            set
            {
                this.sYSCREDITPARENTField = value;
            }
        }

        /// <remarks/>
        public string CREATEDBY
        {
            get
            {
                return this.cREATEDBYField;
            }
            set
            {
                this.cREATEDBYField = value;
            }
        }

        /// <remarks/>
        public object ALTEREDBY
        {
            get
            {
                return this.aLTEREDBYField;
            }
            set
            {
                this.aLTEREDBYField = value;
            }
        }

        /// <remarks/>
        public object TAXCLASSIFICATIONNAME
        {
            get
            {
                return this.tAXCLASSIFICATIONNAMEField;
            }
            set
            {
                this.tAXCLASSIFICATIONNAMEField = value;
            }
        }

        /// <remarks/>
        public string TAXTYPE
        {
            get
            {
                return this.tAXTYPEField;
            }
            set
            {
                this.tAXTYPEField = value;
            }
        }

        /// <remarks/>
        public object BILLCREDITPERIOD
        {
            get
            {
                return this.bILLCREDITPERIODField;
            }
            set
            {
                this.bILLCREDITPERIODField = value;
            }
        }

        /// <remarks/>
        public object BANKDETAILS
        {
            get
            {
                return this.bANKDETAILSField;
            }
            set
            {
                this.bANKDETAILSField = value;
            }
        }

        /// <remarks/>
        public object BANKBRANCHNAME
        {
            get
            {
                return this.bANKBRANCHNAMEField;
            }
            set
            {
                this.bANKBRANCHNAMEField = value;
            }
        }

        /// <remarks/>
        public object BANKBSRCODE
        {
            get
            {
                return this.bANKBSRCODEField;
            }
            set
            {
                this.bANKBSRCODEField = value;
            }
        }

        /// <remarks/>
        public object DEDUCTEETYPE
        {
            get
            {
                return this.dEDUCTEETYPEField;
            }
            set
            {
                this.dEDUCTEETYPEField = value;
            }
        }

        /// <remarks/>
        public object BUSINESSTYPE
        {
            get
            {
                return this.bUSINESSTYPEField;
            }
            set
            {
                this.bUSINESSTYPEField = value;
            }
        }

        /// <remarks/>
        public object TYPEOFNOTIFICATION
        {
            get
            {
                return this.tYPEOFNOTIFICATIONField;
            }
            set
            {
                this.tYPEOFNOTIFICATIONField = value;
            }
        }

        /// <remarks/>
        public object MSMEREGNUMBER
        {
            get
            {
                return this.mSMEREGNUMBERField;
            }
            set
            {
                this.mSMEREGNUMBERField = value;
            }
        }

        /// <remarks/>
        public object COUNTRYOFRESIDENCE
        {
            get
            {
                return this.cOUNTRYOFRESIDENCEField;
            }
            set
            {
                this.cOUNTRYOFRESIDENCEField = value;
            }
        }

        /// <remarks/>
        public object RELATEDPARTYID
        {
            get
            {
                return this.rELATEDPARTYIDField;
            }
            set
            {
                this.rELATEDPARTYIDField = value;
            }
        }

        /// <remarks/>
        public object RELPARTYISSUINGAUTHORITY
        {
            get
            {
                return this.rELPARTYISSUINGAUTHORITYField;
            }
            set
            {
                this.rELPARTYISSUINGAUTHORITYField = value;
            }
        }

        /// <remarks/>
        public object IMPORTEREXPORTERCODE
        {
            get
            {
                return this.iMPORTEREXPORTERCODEField;
            }
            set
            {
                this.iMPORTEREXPORTERCODEField = value;
            }
        }

        /// <remarks/>
        public object EMAILCC
        {
            get
            {
                return this.eMAILCCField;
            }
            set
            {
                this.eMAILCCField = value;
            }
        }

        /// <remarks/>
        public string LEDGERPHONE
        {
            get
            {
                return this.lEDGERPHONEField;
            }
            set
            {
                this.lEDGERPHONEField = value;
            }
        }

        /// <remarks/>
        public object LEDGERFAX
        {
            get
            {
                return this.lEDGERFAXField;
            }
            set
            {
                this.lEDGERFAXField = value;
            }
        }

        /// <remarks/>
        public string LEDGERCONTACT
        {
            get
            {
                return this.lEDGERCONTACTField;
            }
            set
            {
                this.lEDGERCONTACTField = value;
            }
        }

        /// <remarks/>
        public string LEDGERMOBILE
        {
            get
            {
                return this.lEDGERMOBILEField;
            }
            set
            {
                this.lEDGERMOBILEField = value;
            }
        }

        /// <remarks/>
        public object RELATIONTYPE
        {
            get
            {
                return this.rELATIONTYPEField;
            }
            set
            {
                this.rELATIONTYPEField = value;
            }
        }

        /// <remarks/>
        public object BASICTYPEOFDUTY
        {
            get
            {
                return this.bASICTYPEOFDUTYField;
            }
            set
            {
                this.bASICTYPEOFDUTYField = value;
            }
        }

        /// <remarks/>
        public object GSTTYPE
        {
            get
            {
                return this.gSTTYPEField;
            }
            set
            {
                this.gSTTYPEField = value;
            }
        }

        /// <remarks/>
        public object EXEMPTIONTYPE
        {
            get
            {
                return this.eXEMPTIONTYPEField;
            }
            set
            {
                this.eXEMPTIONTYPEField = value;
            }
        }

        /// <remarks/>
        public object APPROPRIATEFOR
        {
            get
            {
                return this.aPPROPRIATEFORField;
            }
            set
            {
                this.aPPROPRIATEFORField = value;
            }
        }

        /// <remarks/>
        public object SUBTAXTYPE
        {
            get
            {
                return this.sUBTAXTYPEField;
            }
            set
            {
                this.sUBTAXTYPEField = value;
            }
        }

        /// <remarks/>
        public object STXNATUREOFPARTY
        {
            get
            {
                return this.sTXNATUREOFPARTYField;
            }
            set
            {
                this.sTXNATUREOFPARTYField = value;
            }
        }

        /// <remarks/>
        public object PAYTYPE
        {
            get
            {
                return this.pAYTYPEField;
            }
            set
            {
                this.pAYTYPEField = value;
            }
        }

        /// <remarks/>
        public object PAYSLIPNAME
        {
            get
            {
                return this.pAYSLIPNAMEField;
            }
            set
            {
                this.pAYSLIPNAMEField = value;
            }
        }

        /// <remarks/>
        public object ATTENDANCETYPE
        {
            get
            {
                return this.aTTENDANCETYPEField;
            }
            set
            {
                this.aTTENDANCETYPEField = value;
            }
        }

        /// <remarks/>
        public object LEAVETYPE
        {
            get
            {
                return this.lEAVETYPEField;
            }
            set
            {
                this.lEAVETYPEField = value;
            }
        }

        /// <remarks/>
        public object CALCULATIONPERIOD
        {
            get
            {
                return this.cALCULATIONPERIODField;
            }
            set
            {
                this.cALCULATIONPERIODField = value;
            }
        }

        /// <remarks/>
        public object ROUNDINGMETHOD
        {
            get
            {
                return this.rOUNDINGMETHODField;
            }
            set
            {
                this.rOUNDINGMETHODField = value;
            }
        }

        /// <remarks/>
        public object COMPUTATIONTYPE
        {
            get
            {
                return this.cOMPUTATIONTYPEField;
            }
            set
            {
                this.cOMPUTATIONTYPEField = value;
            }
        }

        /// <remarks/>
        public object CALCULATIONTYPE
        {
            get
            {
                return this.cALCULATIONTYPEField;
            }
            set
            {
                this.cALCULATIONTYPEField = value;
            }
        }

        /// <remarks/>
        public object PAYSTATTYPE
        {
            get
            {
                return this.pAYSTATTYPEField;
            }
            set
            {
                this.pAYSTATTYPEField = value;
            }
        }

        /// <remarks/>
        public object PROFESSIONALTAXNUMBER
        {
            get
            {
                return this.pROFESSIONALTAXNUMBERField;
            }
            set
            {
                this.pROFESSIONALTAXNUMBERField = value;
            }
        }

        /// <remarks/>
        public object USERDEFINEDCALENDERTYPE
        {
            get
            {
                return this.uSERDEFINEDCALENDERTYPEField;
            }
            set
            {
                this.uSERDEFINEDCALENDERTYPEField = value;
            }
        }

        /// <remarks/>
        public object ITNATURE
        {
            get
            {
                return this.iTNATUREField;
            }
            set
            {
                this.iTNATUREField = value;
            }
        }

        /// <remarks/>
        public object ITCOMPONENT
        {
            get
            {
                return this.iTCOMPONENTField;
            }
            set
            {
                this.iTCOMPONENTField = value;
            }
        }

        /// <remarks/>
        public object NOTIFICATIONNUMBER
        {
            get
            {
                return this.nOTIFICATIONNUMBERField;
            }
            set
            {
                this.nOTIFICATIONNUMBERField = value;
            }
        }

        /// <remarks/>
        public object REGISTRATIONNUMBER
        {
            get
            {
                return this.rEGISTRATIONNUMBERField;
            }
            set
            {
                this.rEGISTRATIONNUMBERField = value;
            }
        }

        /// <remarks/>
        public object SERVICECATEGORY
        {
            get
            {
                return this.sERVICECATEGORYField;
            }
            set
            {
                this.sERVICECATEGORYField = value;
            }
        }

        /// <remarks/>
        public object ABATEMENTNOTIFICATIONNO
        {
            get
            {
                return this.aBATEMENTNOTIFICATIONNOField;
            }
            set
            {
                this.aBATEMENTNOTIFICATIONNOField = value;
            }
        }

        /// <remarks/>
        public object STXDUTYHEAD
        {
            get
            {
                return this.sTXDUTYHEADField;
            }
            set
            {
                this.sTXDUTYHEADField = value;
            }
        }

        /// <remarks/>
        public object STXCLASSIFICATION
        {
            get
            {
                return this.sTXCLASSIFICATIONField;
            }
            set
            {
                this.sTXCLASSIFICATIONField = value;
            }
        }

        /// <remarks/>
        public object EXCISELEDGERCLASSIFICATION
        {
            get
            {
                return this.eXCISELEDGERCLASSIFICATIONField;
            }
            set
            {
                this.eXCISELEDGERCLASSIFICATIONField = value;
            }
        }

        /// <remarks/>
        public object EXCISEREGISTRATIONNUMBER
        {
            get
            {
                return this.eXCISEREGISTRATIONNUMBERField;
            }
            set
            {
                this.eXCISEREGISTRATIONNUMBERField = value;
            }
        }

        /// <remarks/>
        public object EXCISEACCOUNTHEAD
        {
            get
            {
                return this.eXCISEACCOUNTHEADField;
            }
            set
            {
                this.eXCISEACCOUNTHEADField = value;
            }
        }

        /// <remarks/>
        public object EXCISEDUTYTYPE
        {
            get
            {
                return this.eXCISEDUTYTYPEField;
            }
            set
            {
                this.eXCISEDUTYTYPEField = value;
            }
        }

        /// <remarks/>
        public object EXCISEDUTYHEADCODE
        {
            get
            {
                return this.eXCISEDUTYHEADCODEField;
            }
            set
            {
                this.eXCISEDUTYHEADCODEField = value;
            }
        }

        /// <remarks/>
        public object EXCISEALLOCTYPE
        {
            get
            {
                return this.eXCISEALLOCTYPEField;
            }
            set
            {
                this.eXCISEALLOCTYPEField = value;
            }
        }

        /// <remarks/>
        public object EXCISEDUTYHEAD
        {
            get
            {
                return this.eXCISEDUTYHEADField;
            }
            set
            {
                this.eXCISEDUTYHEADField = value;
            }
        }

        /// <remarks/>
        public object NATUREOFSALES
        {
            get
            {
                return this.nATUREOFSALESField;
            }
            set
            {
                this.nATUREOFSALESField = value;
            }
        }

        /// <remarks/>
        public object EXCISENOTIFICATIONNO
        {
            get
            {
                return this.eXCISENOTIFICATIONNOField;
            }
            set
            {
                this.eXCISENOTIFICATIONNOField = value;
            }
        }

        /// <remarks/>
        public object EXCISEREGNO
        {
            get
            {
                return this.eXCISEREGNOField;
            }
            set
            {
                this.eXCISEREGNOField = value;
            }
        }

        /// <remarks/>
        public object EXCISENATUREOFPURCHASE
        {
            get
            {
                return this.eXCISENATUREOFPURCHASEField;
            }
            set
            {
                this.eXCISENATUREOFPURCHASEField = value;
            }
        }

        /// <remarks/>
        public object TDSDEDUCTEETYPE
        {
            get
            {
                return this.tDSDEDUCTEETYPEField;
            }
            set
            {
                this.tDSDEDUCTEETYPEField = value;
            }
        }

        /// <remarks/>
        public object TDSRATENAME
        {
            get
            {
                return this.tDSRATENAMEField;
            }
            set
            {
                this.tDSRATENAMEField = value;
            }
        }

        /// <remarks/>
        public object TDSDEDUCTEESECTIONNUMBER
        {
            get
            {
                return this.tDSDEDUCTEESECTIONNUMBERField;
            }
            set
            {
                this.tDSDEDUCTEESECTIONNUMBERField = value;
            }
        }

        /// <remarks/>
        public object LEDGERFBTCATEGORY
        {
            get
            {
                return this.lEDGERFBTCATEGORYField;
            }
            set
            {
                this.lEDGERFBTCATEGORYField = value;
            }
        }

        /// <remarks/>
        public object BRANCHCODE
        {
            get
            {
                return this.bRANCHCODEField;
            }
            set
            {
                this.bRANCHCODEField = value;
            }
        }

        /// <remarks/>
        public object IFSCODE
        {
            get
            {
                return this.iFSCODEField;
            }
            set
            {
                this.iFSCODEField = value;
            }
        }

        /// <remarks/>
        public object CLIENTCODE
        {
            get
            {
                return this.cLIENTCODEField;
            }
            set
            {
                this.cLIENTCODEField = value;
            }
        }

        /// <remarks/>
        public object BANKINGCONFIGBANK
        {
            get
            {
                return this.bANKINGCONFIGBANKField;
            }
            set
            {
                this.bANKINGCONFIGBANKField = value;
            }
        }

        /// <remarks/>
        public object BANKINGCONFIGBANKID
        {
            get
            {
                return this.bANKINGCONFIGBANKIDField;
            }
            set
            {
                this.bANKINGCONFIGBANKIDField = value;
            }
        }

        /// <remarks/>
        public string ISBILLWISEON
        {
            get
            {
                return this.iSBILLWISEONField;
            }
            set
            {
                this.iSBILLWISEONField = value;
            }
        }

        /// <remarks/>
        public string ISCOSTCENTRESON
        {
            get
            {
                return this.iSCOSTCENTRESONField;
            }
            set
            {
                this.iSCOSTCENTRESONField = value;
            }
        }

        /// <remarks/>
        public string ISINTERESTON
        {
            get
            {
                return this.iSINTERESTONField;
            }
            set
            {
                this.iSINTERESTONField = value;
            }
        }

        /// <remarks/>
        public string ALLOWINMOBILE
        {
            get
            {
                return this.aLLOWINMOBILEField;
            }
            set
            {
                this.aLLOWINMOBILEField = value;
            }
        }

        /// <remarks/>
        public string ISCOSTTRACKINGON
        {
            get
            {
                return this.iSCOSTTRACKINGONField;
            }
            set
            {
                this.iSCOSTTRACKINGONField = value;
            }
        }

        /// <remarks/>
        public string ISCONDENSED
        {
            get
            {
                return this.iSCONDENSEDField;
            }
            set
            {
                this.iSCONDENSEDField = value;
            }
        }

        /// <remarks/>
        public string AFFECTSSTOCK
        {
            get
            {
                return this.aFFECTSSTOCKField;
            }
            set
            {
                this.aFFECTSSTOCKField = value;
            }
        }

        /// <remarks/>
        public string FORPAYROLL
        {
            get
            {
                return this.fORPAYROLLField;
            }
            set
            {
                this.fORPAYROLLField = value;
            }
        }

        /// <remarks/>
        public string ISABCENABLED
        {
            get
            {
                return this.iSABCENABLEDField;
            }
            set
            {
                this.iSABCENABLEDField = value;
            }
        }

        /// <remarks/>
        public string INTERESTONBILLWISE
        {
            get
            {
                return this.iNTERESTONBILLWISEField;
            }
            set
            {
                this.iNTERESTONBILLWISEField = value;
            }
        }

        /// <remarks/>
        public string OVERRIDEINTEREST
        {
            get
            {
                return this.oVERRIDEINTERESTField;
            }
            set
            {
                this.oVERRIDEINTERESTField = value;
            }
        }

        /// <remarks/>
        public string OVERRIDEADVINTEREST
        {
            get
            {
                return this.oVERRIDEADVINTERESTField;
            }
            set
            {
                this.oVERRIDEADVINTERESTField = value;
            }
        }

        /// <remarks/>
        public string USEFORVAT
        {
            get
            {
                return this.uSEFORVATField;
            }
            set
            {
                this.uSEFORVATField = value;
            }
        }

        /// <remarks/>
        public string IGNORETDSEXEMPT
        {
            get
            {
                return this.iGNORETDSEXEMPTField;
            }
            set
            {
                this.iGNORETDSEXEMPTField = value;
            }
        }

        /// <remarks/>
        public string ISTCSAPPLICABLE
        {
            get
            {
                return this.iSTCSAPPLICABLEField;
            }
            set
            {
                this.iSTCSAPPLICABLEField = value;
            }
        }

        /// <remarks/>
        public string ISTDSAPPLICABLE
        {
            get
            {
                return this.iSTDSAPPLICABLEField;
            }
            set
            {
                this.iSTDSAPPLICABLEField = value;
            }
        }

        /// <remarks/>
        public string ISFBTAPPLICABLE
        {
            get
            {
                return this.iSFBTAPPLICABLEField;
            }
            set
            {
                this.iSFBTAPPLICABLEField = value;
            }
        }

        /// <remarks/>
        public string ISGSTAPPLICABLE
        {
            get
            {
                return this.iSGSTAPPLICABLEField;
            }
            set
            {
                this.iSGSTAPPLICABLEField = value;
            }
        }

        /// <remarks/>
        public string ISEXCISEAPPLICABLE
        {
            get
            {
                return this.iSEXCISEAPPLICABLEField;
            }
            set
            {
                this.iSEXCISEAPPLICABLEField = value;
            }
        }

        /// <remarks/>
        public string ISTDSEXPENSE
        {
            get
            {
                return this.iSTDSEXPENSEField;
            }
            set
            {
                this.iSTDSEXPENSEField = value;
            }
        }

        /// <remarks/>
        public string ISEDLIAPPLICABLE
        {
            get
            {
                return this.iSEDLIAPPLICABLEField;
            }
            set
            {
                this.iSEDLIAPPLICABLEField = value;
            }
        }

        /// <remarks/>
        public string ISRELATEDPARTY
        {
            get
            {
                return this.iSRELATEDPARTYField;
            }
            set
            {
                this.iSRELATEDPARTYField = value;
            }
        }

        /// <remarks/>
        public string USEFORESIELIGIBILITY
        {
            get
            {
                return this.uSEFORESIELIGIBILITYField;
            }
            set
            {
                this.uSEFORESIELIGIBILITYField = value;
            }
        }

        /// <remarks/>
        public string SHOWINPAYSLIP
        {
            get
            {
                return this.sHOWINPAYSLIPField;
            }
            set
            {
                this.sHOWINPAYSLIPField = value;
            }
        }

        /// <remarks/>
        public string USEFORGRATUITY
        {
            get
            {
                return this.uSEFORGRATUITYField;
            }
            set
            {
                this.uSEFORGRATUITYField = value;
            }
        }

        /// <remarks/>
        public string ISTDSPROJECTED
        {
            get
            {
                return this.iSTDSPROJECTEDField;
            }
            set
            {
                this.iSTDSPROJECTEDField = value;
            }
        }

        /// <remarks/>
        public string FORSERVICETAX
        {
            get
            {
                return this.fORSERVICETAXField;
            }
            set
            {
                this.fORSERVICETAXField = value;
            }
        }

        /// <remarks/>
        public string ISINPUTCREDIT
        {
            get
            {
                return this.iSINPUTCREDITField;
            }
            set
            {
                this.iSINPUTCREDITField = value;
            }
        }

        /// <remarks/>
        public string ISEXEMPTED
        {
            get
            {
                return this.iSEXEMPTEDField;
            }
            set
            {
                this.iSEXEMPTEDField = value;
            }
        }

        /// <remarks/>
        public string ISABATEMENTAPPLICABLE
        {
            get
            {
                return this.iSABATEMENTAPPLICABLEField;
            }
            set
            {
                this.iSABATEMENTAPPLICABLEField = value;
            }
        }

        /// <remarks/>
        public string ISSTXPARTY
        {
            get
            {
                return this.iSSTXPARTYField;
            }
            set
            {
                this.iSSTXPARTYField = value;
            }
        }

        /// <remarks/>
        public string ISSTXNONREALIZEDTYPE
        {
            get
            {
                return this.iSSTXNONREALIZEDTYPEField;
            }
            set
            {
                this.iSSTXNONREALIZEDTYPEField = value;
            }
        }

        /// <remarks/>
        public string TDSDEDUCTEEISSPECIALRATE
        {
            get
            {
                return this.tDSDEDUCTEEISSPECIALRATEField;
            }
            set
            {
                this.tDSDEDUCTEEISSPECIALRATEField = value;
            }
        }

        /// <remarks/>
        public string AUDITED
        {
            get
            {
                return this.aUDITEDField;
            }
            set
            {
                this.aUDITEDField = value;
            }
        }

        /// <remarks/>
        public byte SAMPLINGNUMONEFACTOR
        {
            get
            {
                return this.sAMPLINGNUMONEFACTORField;
            }
            set
            {
                this.sAMPLINGNUMONEFACTORField = value;
            }
        }

        /// <remarks/>
        public byte SAMPLINGNUMTWOFACTOR
        {
            get
            {
                return this.sAMPLINGNUMTWOFACTORField;
            }
            set
            {
                this.sAMPLINGNUMTWOFACTORField = value;
            }
        }

        /// <remarks/>
        public ushort SORTPOSITION
        {
            get
            {
                return this.sORTPOSITIONField;
            }
            set
            {
                this.sORTPOSITIONField = value;
            }
        }

        /// <remarks/>
        public byte DEFAULTLANGUAGE
        {
            get
            {
                return this.dEFAULTLANGUAGEField;
            }
            set
            {
                this.dEFAULTLANGUAGEField = value;
            }
        }

        /// <remarks/>
        public byte RATEOFTAXCALCULATION
        {
            get
            {
                return this.rATEOFTAXCALCULATIONField;
            }
            set
            {
                this.rATEOFTAXCALCULATIONField = value;
            }
        }

        /// <remarks/>
        public byte GRATUITYMONTHDAYS
        {
            get
            {
                return this.gRATUITYMONTHDAYSField;
            }
            set
            {
                this.gRATUITYMONTHDAYSField = value;
            }
        }

        /// <remarks/>
        public byte GRATUITYLIMITMONTHS
        {
            get
            {
                return this.gRATUITYLIMITMONTHSField;
            }
            set
            {
                this.gRATUITYLIMITMONTHSField = value;
            }
        }

        /// <remarks/>
        public byte CALCULATIONBASIS
        {
            get
            {
                return this.cALCULATIONBASISField;
            }
            set
            {
                this.cALCULATIONBASISField = value;
            }
        }

        /// <remarks/>
        public byte ROUNDINGLIMIT
        {
            get
            {
                return this.rOUNDINGLIMITField;
            }
            set
            {
                this.rOUNDINGLIMITField = value;
            }
        }

        /// <remarks/>
        public byte ABATEMENTPERCENTAGE
        {
            get
            {
                return this.aBATEMENTPERCENTAGEField;
            }
            set
            {
                this.aBATEMENTPERCENTAGEField = value;
            }
        }

        /// <remarks/>
        public byte TDSDEDUCTEESPECIALRATE
        {
            get
            {
                return this.tDSDEDUCTEESPECIALRATEField;
            }
            set
            {
                this.tDSDEDUCTEESPECIALRATEField = value;
            }
        }

        /// <remarks/>
        public object SAMPLINGAMTONEFACTOR
        {
            get
            {
                return this.sAMPLINGAMTONEFACTORField;
            }
            set
            {
                this.sAMPLINGAMTONEFACTORField = value;
            }
        }

        /// <remarks/>
        public object SAMPLINGAMTTWOFACTOR
        {
            get
            {
                return this.sAMPLINGAMTTWOFACTORField;
            }
            set
            {
                this.sAMPLINGAMTTWOFACTORField = value;
            }
        }

        /// <remarks/>
        public string OPENINGBALANCE
        {
            get
            {
                return this.oPENINGBALANCEField;
            }
            set
            {
                this.oPENINGBALANCEField = value;
            }
        }

        /// <remarks/>
        public object CREDITLIMIT
        {
            get
            {
                return this.cREDITLIMITField;
            }
            set
            {
                this.cREDITLIMITField = value;
            }
        }

        /// <remarks/>
        public object GRATUITYLIMITAMOUNT
        {
            get
            {
                return this.gRATUITYLIMITAMOUNTField;
            }
            set
            {
                this.gRATUITYLIMITAMOUNTField = value;
            }
        }

        /// <remarks/>
        public object ODLIMIT
        {
            get
            {
                return this.oDLIMITField;
            }
            set
            {
                this.oDLIMITField = value;
            }
        }

        /// <remarks/>
        public object XBRLPREVYRLEDGERAMT
        {
            get
            {
                return this.xBRLPREVYRLEDGERAMTField;
            }
            set
            {
                this.xBRLPREVYRLEDGERAMTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LANGUAGENAME.LIST")]
        public ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGERLANGUAGENAMELIST LANGUAGENAMELIST
        {
            get
            {
                return this.lANGUAGENAMELISTField;
            }
            set
            {
                this.lANGUAGENAMELISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("XBRLDETAIL.LIST")]
        public object XBRLDETAILLIST
        {
            get
            {
                return this.xBRLDETAILLISTField;
            }
            set
            {
                this.xBRLDETAILLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AUDITDETAILS.LIST")]
        public object AUDITDETAILSLIST
        {
            get
            {
                return this.aUDITDETAILSLISTField;
            }
            set
            {
                this.aUDITDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SCHVIDETAILS.LIST")]
        public object SCHVIDETAILSLIST
        {
            get
            {
                return this.sCHVIDETAILSLISTField;
            }
            set
            {
                this.sCHVIDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SLABPERIOD.LIST")]
        public object SLABPERIODLIST
        {
            get
            {
                return this.sLABPERIODLISTField;
            }
            set
            {
                this.sLABPERIODLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("GRATUITYPERIOD.LIST")]
        public object GRATUITYPERIODLIST
        {
            get
            {
                return this.gRATUITYPERIODLISTField;
            }
            set
            {
                this.gRATUITYPERIODLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ADDITIONALCOMPUTATIONS.LIST")]
        public object ADDITIONALCOMPUTATIONSLIST
        {
            get
            {
                return this.aDDITIONALCOMPUTATIONSLISTField;
            }
            set
            {
                this.aDDITIONALCOMPUTATIONSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("BANKALLOCATIONS.LIST")]
        public object BANKALLOCATIONSLIST
        {
            get
            {
                return this.bANKALLOCATIONSLISTField;
            }
            set
            {
                this.bANKALLOCATIONSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PAYMENTDETAILS.LIST")]
        public object PAYMENTDETAILSLIST
        {
            get
            {
                return this.pAYMENTDETAILSLISTField;
            }
            set
            {
                this.pAYMENTDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("BANKEXPORTFORMATS.LIST")]
        public object BANKEXPORTFORMATSLIST
        {
            get
            {
                return this.bANKEXPORTFORMATSLISTField;
            }
            set
            {
                this.bANKEXPORTFORMATSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("BILLALLOCATIONS.LIST")]
        public object BILLALLOCATIONSLIST
        {
            get
            {
                return this.bILLALLOCATIONSLISTField;
            }
            set
            {
                this.bILLALLOCATIONSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("INTERESTCOLLECTION.LIST")]
        public object INTERESTCOLLECTIONLIST
        {
            get
            {
                return this.iNTERESTCOLLECTIONLISTField;
            }
            set
            {
                this.iNTERESTCOLLECTIONLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LEDGERCLOSINGVALUES.LIST")]
        public object LEDGERCLOSINGVALUESLIST
        {
            get
            {
                return this.lEDGERCLOSINGVALUESLISTField;
            }
            set
            {
                this.lEDGERCLOSINGVALUESLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LEDGERAUDITCLASS.LIST")]
        public object LEDGERAUDITCLASSLIST
        {
            get
            {
                return this.lEDGERAUDITCLASSLISTField;
            }
            set
            {
                this.lEDGERAUDITCLASSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("OLDAUDITENTRIES.LIST")]
        public object OLDAUDITENTRIESLIST
        {
            get
            {
                return this.oLDAUDITENTRIESLISTField;
            }
            set
            {
                this.oLDAUDITENTRIESLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TDSEXEMPTIONRULES.LIST")]
        public object TDSEXEMPTIONRULESLIST
        {
            get
            {
                return this.tDSEXEMPTIONRULESLISTField;
            }
            set
            {
                this.tDSEXEMPTIONRULESLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DEDUCTINSAMEVCHRULES.LIST")]
        public object DEDUCTINSAMEVCHRULESLIST
        {
            get
            {
                return this.dEDUCTINSAMEVCHRULESLISTField;
            }
            set
            {
                this.dEDUCTINSAMEVCHRULESLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LOWERDEDUCTION.LIST")]
        public object LOWERDEDUCTIONLIST
        {
            get
            {
                return this.lOWERDEDUCTIONLISTField;
            }
            set
            {
                this.lOWERDEDUCTIONLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("STXABATEMENTDETAILS.LIST")]
        public object STXABATEMENTDETAILSLIST
        {
            get
            {
                return this.sTXABATEMENTDETAILSLISTField;
            }
            set
            {
                this.sTXABATEMENTDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LEDMULTIADDRESSLIST.LIST")]
        public object LEDMULTIADDRESSLISTLIST
        {
            get
            {
                return this.lEDMULTIADDRESSLISTLISTField;
            }
            set
            {
                this.lEDMULTIADDRESSLISTLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("STXTAXDETAILS.LIST")]
        public object STXTAXDETAILSLIST
        {
            get
            {
                return this.sTXTAXDETAILSLISTField;
            }
            set
            {
                this.sTXTAXDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CHEQUERANGE.LIST")]
        public object CHEQUERANGELIST
        {
            get
            {
                return this.cHEQUERANGELISTField;
            }
            set
            {
                this.cHEQUERANGELISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DEFAULTVCHCHEQUEDETAILS.LIST")]
        public object DEFAULTVCHCHEQUEDETAILSLIST
        {
            get
            {
                return this.dEFAULTVCHCHEQUEDETAILSLISTField;
            }
            set
            {
                this.dEFAULTVCHCHEQUEDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ACCOUNTAUDITENTRIES.LIST")]
        public object ACCOUNTAUDITENTRIESLIST
        {
            get
            {
                return this.aCCOUNTAUDITENTRIESLISTField;
            }
            set
            {
                this.aCCOUNTAUDITENTRIESLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AUDITENTRIES.LIST")]
        public object AUDITENTRIESLIST
        {
            get
            {
                return this.aUDITENTRIESLISTField;
            }
            set
            {
                this.aUDITENTRIESLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("BRSIMPORTEDINFO.LIST")]
        public object BRSIMPORTEDINFOLIST
        {
            get
            {
                return this.bRSIMPORTEDINFOLISTField;
            }
            set
            {
                this.bRSIMPORTEDINFOLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AUTOBRSCONFIGS.LIST")]
        public object AUTOBRSCONFIGSLIST
        {
            get
            {
                return this.aUTOBRSCONFIGSLISTField;
            }
            set
            {
                this.aUTOBRSCONFIGSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("BANKURENTRIES.LIST")]
        public object BANKURENTRIESLIST
        {
            get
            {
                return this.bANKURENTRIESLISTField;
            }
            set
            {
                this.bANKURENTRIESLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DEFAULTCHEQUEDETAILS.LIST")]
        public object DEFAULTCHEQUEDETAILSLIST
        {
            get
            {
                return this.dEFAULTCHEQUEDETAILSLISTField;
            }
            set
            {
                this.dEFAULTCHEQUEDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DEFAULTOPENINGCHEQUEDETAILS.LIST")]
        public object DEFAULTOPENINGCHEQUEDETAILSLIST
        {
            get
            {
                return this.dEFAULTOPENINGCHEQUEDETAILSLISTField;
            }
            set
            {
                this.dEFAULTOPENINGCHEQUEDETAILSLISTField = value;
            }
        }



        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NAME
        {
            get
            {
                return this.nAMEField;
            }
            set
            {
                this.nAMEField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlAttributeAttribute()]
        //public string EMAIL
        //{
        //    get
        //    {
        //        return this.eMAILField;
        //    }
        //    set
        //    {
        //        this.eMAILField = value;
        //    }
        //}


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RESERVEDNAME
        {
            get
            {
                return this.rESERVEDNAMEField;
            }
            set
            {
                this.rESERVEDNAMEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGERADDRESSLIST
    {

        private string[] aDDRESSField;

        private string tYPEField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ADDRESS")]
        public string[] ADDRESS
        {
            get
            {
                return this.aDDRESSField;
            }
            set
            {
                this.aDDRESSField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGERMAILINGNAMELIST
    {

        private string mAILINGNAMEField;

        private string tYPEField;

        /// <remarks/>
        public string MAILINGNAME
        {
            get
            {
                return this.mAILINGNAMEField;
            }
            set
            {
                this.mAILINGNAMEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGEROLDAUDITENTRYIDSLIST
    {

        private sbyte oLDAUDITENTRYIDSField;

        private string tYPEField;

        /// <remarks/>
        public sbyte OLDAUDITENTRYIDS
        {
            get
            {
                return this.oLDAUDITENTRYIDSField;
            }
            set
            {
                this.oLDAUDITENTRYIDSField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGERLANGUAGENAMELIST
    {

        private ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGERLANGUAGENAMELISTNAMELIST nAMELISTField;

        private ushort lANGUAGEIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("NAME.LIST")]
        public ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGERLANGUAGENAMELISTNAMELIST NAMELIST
        {
            get
            {
                return this.nAMELISTField;
            }
            set
            {
                this.nAMELISTField = value;
            }
        }

        /// <remarks/>
        public ushort LANGUAGEID
        {
            get
            {
                return this.lANGUAGEIDField;
            }
            set
            {
                this.lANGUAGEIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGERLANGUAGENAMELISTNAMELIST
    {

        private string[] nAMEField;

        private string tYPEField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("NAME")]
        public string[] NAME
        {
            get
            {
                return this.nAMEField;
            }
            set
            {
                this.nAMEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATATALLYMESSAGEGROUP
    {

        private object sAMPLINGDATEONEFACTORField;

        private object sAMPLINGDATETWOFACTORField;

        private object aCTIVEFROMField;

        private object aCTIVETOField;

        private string pARENTField;

        private object sAMPLINGMETHODField;

        private object sAMPLINGSTRONEFACTORField;

        private object nARRATIONField;

        private string bASICGROUPISCALCULABLEField;

        private object aDDLALLOCTYPEField;

        private object gRPDEBITPARENTField;

        private object gRPCREDITPARENTField;

        private object sYSDEBITPARENTField;

        private object sYSCREDITPARENTField;

        private object bALANCINGTYPEField;

        private string iSBILLWISEONField;

        private string iSCOSTCENTRESONField;

        private string iSADDABLEField;

        private string iSSUBLEDGERField;

        private string iSREVENUEField;

        private string aFFECTSGROSSPROFITField;

        private string iSDEEMEDPOSITIVEField;

        private string tRACKNEGATIVEBALANCESField;

        private string iSCONDENSEDField;

        private string aFFECTSSTOCKField;

        private string iSGROUPFORLOANRCPTField;

        private string iSGROUPFORLOANPYMNTField;

        private byte sAMPLINGNUMONEFACTORField;

        private byte sAMPLINGNUMTWOFACTORField;

        private ushort sORTPOSITIONField;

        private object sAMPLINGAMTONEFACTORField;

        private object sAMPLINGAMTTWOFACTORField;

        private ENVELOPEBODYIMPORTDATATALLYMESSAGEGROUPLANGUAGENAMELIST lANGUAGENAMELISTField;

        private object xBRLDETAILLISTField;

        private object aUDITDETAILSLISTField;

        private object sCHVIDETAILSLISTField;

        private string nAMEField;

        private string rESERVEDNAMEField;

        /// <remarks/>
        public object SAMPLINGDATEONEFACTOR
        {
            get
            {
                return this.sAMPLINGDATEONEFACTORField;
            }
            set
            {
                this.sAMPLINGDATEONEFACTORField = value;
            }
        }

        /// <remarks/>
        public object SAMPLINGDATETWOFACTOR
        {
            get
            {
                return this.sAMPLINGDATETWOFACTORField;
            }
            set
            {
                this.sAMPLINGDATETWOFACTORField = value;
            }
        }

        /// <remarks/>
        public object ACTIVEFROM
        {
            get
            {
                return this.aCTIVEFROMField;
            }
            set
            {
                this.aCTIVEFROMField = value;
            }
        }

        /// <remarks/>
        public object ACTIVETO
        {
            get
            {
                return this.aCTIVETOField;
            }
            set
            {
                this.aCTIVETOField = value;
            }
        }

        /// <remarks/>
        public string PARENT
        {
            get
            {
                return this.pARENTField;
            }
            set
            {
                this.pARENTField = value;
            }
        }

        /// <remarks/>
        public object SAMPLINGMETHOD
        {
            get
            {
                return this.sAMPLINGMETHODField;
            }
            set
            {
                this.sAMPLINGMETHODField = value;
            }
        }

        /// <remarks/>
        public object SAMPLINGSTRONEFACTOR
        {
            get
            {
                return this.sAMPLINGSTRONEFACTORField;
            }
            set
            {
                this.sAMPLINGSTRONEFACTORField = value;
            }
        }

        /// <remarks/>
        public object NARRATION
        {
            get
            {
                return this.nARRATIONField;
            }
            set
            {
                this.nARRATIONField = value;
            }
        }

        /// <remarks/>
        public string BASICGROUPISCALCULABLE
        {
            get
            {
                return this.bASICGROUPISCALCULABLEField;
            }
            set
            {
                this.bASICGROUPISCALCULABLEField = value;
            }
        }

        /// <remarks/>
        public object ADDLALLOCTYPE
        {
            get
            {
                return this.aDDLALLOCTYPEField;
            }
            set
            {
                this.aDDLALLOCTYPEField = value;
            }
        }

        /// <remarks/>
        public object GRPDEBITPARENT
        {
            get
            {
                return this.gRPDEBITPARENTField;
            }
            set
            {
                this.gRPDEBITPARENTField = value;
            }
        }

        /// <remarks/>
        public object GRPCREDITPARENT
        {
            get
            {
                return this.gRPCREDITPARENTField;
            }
            set
            {
                this.gRPCREDITPARENTField = value;
            }
        }

        /// <remarks/>
        public object SYSDEBITPARENT
        {
            get
            {
                return this.sYSDEBITPARENTField;
            }
            set
            {
                this.sYSDEBITPARENTField = value;
            }
        }

        /// <remarks/>
        public object SYSCREDITPARENT
        {
            get
            {
                return this.sYSCREDITPARENTField;
            }
            set
            {
                this.sYSCREDITPARENTField = value;
            }
        }

        /// <remarks/>
        public object BALANCINGTYPE
        {
            get
            {
                return this.bALANCINGTYPEField;
            }
            set
            {
                this.bALANCINGTYPEField = value;
            }
        }

        /// <remarks/>
        public string ISBILLWISEON
        {
            get
            {
                return this.iSBILLWISEONField;
            }
            set
            {
                this.iSBILLWISEONField = value;
            }
        }

        /// <remarks/>
        public string ISCOSTCENTRESON
        {
            get
            {
                return this.iSCOSTCENTRESONField;
            }
            set
            {
                this.iSCOSTCENTRESONField = value;
            }
        }

        /// <remarks/>
        public string ISADDABLE
        {
            get
            {
                return this.iSADDABLEField;
            }
            set
            {
                this.iSADDABLEField = value;
            }
        }

        /// <remarks/>
        public string ISSUBLEDGER
        {
            get
            {
                return this.iSSUBLEDGERField;
            }
            set
            {
                this.iSSUBLEDGERField = value;
            }
        }

        /// <remarks/>
        public string ISREVENUE
        {
            get
            {
                return this.iSREVENUEField;
            }
            set
            {
                this.iSREVENUEField = value;
            }
        }

        /// <remarks/>
        public string AFFECTSGROSSPROFIT
        {
            get
            {
                return this.aFFECTSGROSSPROFITField;
            }
            set
            {
                this.aFFECTSGROSSPROFITField = value;
            }
        }

        /// <remarks/>
        public string ISDEEMEDPOSITIVE
        {
            get
            {
                return this.iSDEEMEDPOSITIVEField;
            }
            set
            {
                this.iSDEEMEDPOSITIVEField = value;
            }
        }

        /// <remarks/>
        public string TRACKNEGATIVEBALANCES
        {
            get
            {
                return this.tRACKNEGATIVEBALANCESField;
            }
            set
            {
                this.tRACKNEGATIVEBALANCESField = value;
            }
        }

        /// <remarks/>
        public string ISCONDENSED
        {
            get
            {
                return this.iSCONDENSEDField;
            }
            set
            {
                this.iSCONDENSEDField = value;
            }
        }

        /// <remarks/>
        public string AFFECTSSTOCK
        {
            get
            {
                return this.aFFECTSSTOCKField;
            }
            set
            {
                this.aFFECTSSTOCKField = value;
            }
        }

        /// <remarks/>
        public string ISGROUPFORLOANRCPT
        {
            get
            {
                return this.iSGROUPFORLOANRCPTField;
            }
            set
            {
                this.iSGROUPFORLOANRCPTField = value;
            }
        }

        /// <remarks/>
        public string ISGROUPFORLOANPYMNT
        {
            get
            {
                return this.iSGROUPFORLOANPYMNTField;
            }
            set
            {
                this.iSGROUPFORLOANPYMNTField = value;
            }
        }

        /// <remarks/>
        public byte SAMPLINGNUMONEFACTOR
        {
            get
            {
                return this.sAMPLINGNUMONEFACTORField;
            }
            set
            {
                this.sAMPLINGNUMONEFACTORField = value;
            }
        }

        /// <remarks/>
        public byte SAMPLINGNUMTWOFACTOR
        {
            get
            {
                return this.sAMPLINGNUMTWOFACTORField;
            }
            set
            {
                this.sAMPLINGNUMTWOFACTORField = value;
            }
        }

        /// <remarks/>
        public ushort SORTPOSITION
        {
            get
            {
                return this.sORTPOSITIONField;
            }
            set
            {
                this.sORTPOSITIONField = value;
            }
        }

        /// <remarks/>
        public object SAMPLINGAMTONEFACTOR
        {
            get
            {
                return this.sAMPLINGAMTONEFACTORField;
            }
            set
            {
                this.sAMPLINGAMTONEFACTORField = value;
            }
        }

        /// <remarks/>
        public object SAMPLINGAMTTWOFACTOR
        {
            get
            {
                return this.sAMPLINGAMTTWOFACTORField;
            }
            set
            {
                this.sAMPLINGAMTTWOFACTORField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LANGUAGENAME.LIST")]
        public ENVELOPEBODYIMPORTDATATALLYMESSAGEGROUPLANGUAGENAMELIST LANGUAGENAMELIST
        {
            get
            {
                return this.lANGUAGENAMELISTField;
            }
            set
            {
                this.lANGUAGENAMELISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("XBRLDETAIL.LIST")]
        public object XBRLDETAILLIST
        {
            get
            {
                return this.xBRLDETAILLISTField;
            }
            set
            {
                this.xBRLDETAILLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AUDITDETAILS.LIST")]
        public object AUDITDETAILSLIST
        {
            get
            {
                return this.aUDITDETAILSLISTField;
            }
            set
            {
                this.aUDITDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SCHVIDETAILS.LIST")]
        public object SCHVIDETAILSLIST
        {
            get
            {
                return this.sCHVIDETAILSLISTField;
            }
            set
            {
                this.sCHVIDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NAME
        {
            get
            {
                return this.nAMEField;
            }
            set
            {
                this.nAMEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RESERVEDNAME
        {
            get
            {
                return this.rESERVEDNAMEField;
            }
            set
            {
                this.rESERVEDNAMEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATATALLYMESSAGEGROUPLANGUAGENAMELIST
    {

        private ENVELOPEBODYIMPORTDATATALLYMESSAGEGROUPLANGUAGENAMELISTNAMELIST nAMELISTField;

        private ushort lANGUAGEIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("NAME.LIST")]
        public ENVELOPEBODYIMPORTDATATALLYMESSAGEGROUPLANGUAGENAMELISTNAMELIST NAMELIST
        {
            get
            {
                return this.nAMELISTField;
            }
            set
            {
                this.nAMELISTField = value;
            }
        }

        /// <remarks/>
        public ushort LANGUAGEID
        {
            get
            {
                return this.lANGUAGEIDField;
            }
            set
            {
                this.lANGUAGEIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATATALLYMESSAGEGROUPLANGUAGENAMELISTNAMELIST
    {

        private string[] nAMEField;

        private string tYPEField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("NAME")]
        public string[] NAME
        {
            get
            {
                return this.nAMEField;
            }
            set
            {
                this.nAMEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATATALLYMESSAGECURRENCY
    {

        private object aCTIVEFROMField;

        private object aCTIVETOField;

        private string mAILINGNAMEField;

        private object nARRATIONField;

        private string oRIGINALNAMEField;

        private string eXPANDEDSYMBOLField;

        private string dECIMALSYMBOLField;

        private string iSSUFFIXField;

        private string hASSPACEField;

        private string iNMILLIONSField;

        private byte sORTPOSITIONField;

        private byte dECIMALPLACESField;

        private byte dECIMALPLACESFORPRINTINGField;

        private object dAILYSTDRATESLISTField;

        private object dAILYBUYINGRATESLISTField;

        private object dAILYSELLINGRATESLISTField;

        private string nAMEField;

        private string rESERVEDNAMEField;

        /// <remarks/>
        public object ACTIVEFROM
        {
            get
            {
                return this.aCTIVEFROMField;
            }
            set
            {
                this.aCTIVEFROMField = value;
            }
        }

        /// <remarks/>
        public object ACTIVETO
        {
            get
            {
                return this.aCTIVETOField;
            }
            set
            {
                this.aCTIVETOField = value;
            }
        }

        /// <remarks/>
        public string MAILINGNAME
        {
            get
            {
                return this.mAILINGNAMEField;
            }
            set
            {
                this.mAILINGNAMEField = value;
            }
        }

        /// <remarks/>
        public object NARRATION
        {
            get
            {
                return this.nARRATIONField;
            }
            set
            {
                this.nARRATIONField = value;
            }
        }

        /// <remarks/>
        public string ORIGINALNAME
        {
            get
            {
                return this.oRIGINALNAMEField;
            }
            set
            {
                this.oRIGINALNAMEField = value;
            }
        }

        /// <remarks/>
        public string EXPANDEDSYMBOL
        {
            get
            {
                return this.eXPANDEDSYMBOLField;
            }
            set
            {
                this.eXPANDEDSYMBOLField = value;
            }
        }

        /// <remarks/>
        public string DECIMALSYMBOL
        {
            get
            {
                return this.dECIMALSYMBOLField;
            }
            set
            {
                this.dECIMALSYMBOLField = value;
            }
        }

        /// <remarks/>
        public string ISSUFFIX
        {
            get
            {
                return this.iSSUFFIXField;
            }
            set
            {
                this.iSSUFFIXField = value;
            }
        }

        /// <remarks/>
        public string HASSPACE
        {
            get
            {
                return this.hASSPACEField;
            }
            set
            {
                this.hASSPACEField = value;
            }
        }

        /// <remarks/>
        public string INMILLIONS
        {
            get
            {
                return this.iNMILLIONSField;
            }
            set
            {
                this.iNMILLIONSField = value;
            }
        }

        /// <remarks/>
        public byte SORTPOSITION
        {
            get
            {
                return this.sORTPOSITIONField;
            }
            set
            {
                this.sORTPOSITIONField = value;
            }
        }

        /// <remarks/>
        public byte DECIMALPLACES
        {
            get
            {
                return this.dECIMALPLACESField;
            }
            set
            {
                this.dECIMALPLACESField = value;
            }
        }

        /// <remarks/>
        public byte DECIMALPLACESFORPRINTING
        {
            get
            {
                return this.dECIMALPLACESFORPRINTINGField;
            }
            set
            {
                this.dECIMALPLACESFORPRINTINGField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DAILYSTDRATES.LIST")]
        public object DAILYSTDRATESLIST
        {
            get
            {
                return this.dAILYSTDRATESLISTField;
            }
            set
            {
                this.dAILYSTDRATESLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DAILYBUYINGRATES.LIST")]
        public object DAILYBUYINGRATESLIST
        {
            get
            {
                return this.dAILYBUYINGRATESLISTField;
            }
            set
            {
                this.dAILYBUYINGRATESLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DAILYSELLINGRATES.LIST")]
        public object DAILYSELLINGRATESLIST
        {
            get
            {
                return this.dAILYSELLINGRATESLISTField;
            }
            set
            {
                this.dAILYSELLINGRATESLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NAME
        {
            get
            {
                return this.nAMEField;
            }
            set
            {
                this.nAMEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RESERVEDNAME
        {
            get
            {
                return this.rESERVEDNAMEField;
            }
            set
            {
                this.rESERVEDNAMEField = value;
            }
        }
    }


}
