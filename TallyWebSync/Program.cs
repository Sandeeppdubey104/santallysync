﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TallyWebSync
{
    class Program
    {
        static void Main(string[] args)
        {

            var home = new TallyWebSyncHome();
            home.SendServicesRequestAsync();
            Console.ReadKey();
        }
    }
}
