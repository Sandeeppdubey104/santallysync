﻿

namespace TallyWebSync.Modal
{
   public class TransactionList
    {
        public string TRANS_NO { get; set; }
        public double AMOUNT { get; set; }
        public string PAY_MODE { get; set; }
        public string INVOICE { get; set; }
    }
}
