﻿namespace TallyWebSync
{
    public class CustomreList
    {
        public string fname { get; set; }
        public string lname { get; set; }
        public string contact_name { get; set; }
        public string customer_code { get; set; }
        
        public string address { get; set; }
        public string mobile { get; set; }
        public string phone { get; set; }

        public string email { get; set; }
        public string gstin { get; set; }
        public string coach { get; set; }
        public string salesperson { get; set; }
        public string type { get; set; }
        public string status { get; set; }
        public string created_by { get; set; }
        public string remarks { get; set; }
        public string created_on { get; set; }

    }

}
