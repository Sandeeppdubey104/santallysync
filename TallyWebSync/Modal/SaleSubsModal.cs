﻿

namespace TallyWebSync.Modal
{
    public class SaleSubsList
    {

       public string PRODUCT_CODE { get; set; }
       public double CUSTOMER_CODE { get; set; }
       public string VALIDITY { get; set; }
       public string RENEWAL_COST { get; set; }
       public string PRODUCT_PRICE { get; set; }
       public string CREATED_AT { get; set; }
       public string UPDATED_AT { get; set; }
    }
}
