﻿

namespace TallyWebSync
{

    
    public partial class ENVELOPE
    {

         
        /// <remarks/>
       
    }


   
 

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATATALLYMESSAGECOMPANY
    {

        private ENVELOPEBODYIMPORTDATATALLYMESSAGECOMPANYREMOTECMPINFOLIST rEMOTECMPINFOLISTField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("REMOTECMPINFO.LIST")]
        public ENVELOPEBODYIMPORTDATATALLYMESSAGECOMPANYREMOTECMPINFOLIST REMOTECMPINFOLIST
        {
            get
            {
                return this.rEMOTECMPINFOLISTField;
            }
            set
            {
                this.rEMOTECMPINFOLISTField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATATALLYMESSAGECOMPANYREMOTECMPINFOLIST
    {

        private string nAMEField;

        private string rEMOTECMPNAMEField;

        private string rEMOTECMPSTATEField;

        private string mERGEField;

        /// <remarks/>
        public string NAME
        {
            get
            {
                return this.nAMEField;
            }
            set
            {
                this.nAMEField = value;
            }
        }

        /// <remarks/>
        public string REMOTECMPNAME
        {
            get
            {
                return this.rEMOTECMPNAMEField;
            }
            set
            {
                this.rEMOTECMPNAMEField = value;
            }
        }

        /// <remarks/>
        public string REMOTECMPSTATE
        {
            get
            {
                return this.rEMOTECMPSTATEField;
            }
            set
            {
                this.rEMOTECMPSTATEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MERGE
        {
            get
            {
                return this.mERGEField;
            }
            set
            {
                this.mERGEField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ENVELOPEBODYIMPORTDATATALLYMESSAGELEDGER
    {

        private object vATAPPLICABLEDATEField;

        private object pANAPPLICABLEFROMField;

        private object pAYINSRUNNINGFILEDATEField;

        private object vATTAXEXEMPTIONDATEField;

        private string gUIDField;



        private object wEBSITEField;

        private string cOUNTRYNAMEField;

        private object lBTREGNNOField;

        private object lBTZONEField;

        private object eXPORTIMPORTCODEField;

        private string gSTREGISTRATIONTYPEField;

        

        private object uPLOADLASTREFRESHField;

        

        private object rEQUESTORRULEField;

        

        private object tDSAPPLICABLEField;

        private object tCSAPPLICABLEField;

        private object gSTAPPLICABLEField;

      

       

        private object dESCRIPTIONField;

        private object lEDADDLALLOCTYPEField;

        private object tRANSPORTERIDField;

       

        private object mAILINGNAMENATIVEField;

        private object sTATENAMENATIVEField;

        private object cOUNTRYNAMENATIVEField;

         

        private object nAMEONPANField;

        private object uSEDFORTAXTYPEField;

        private object eCOMMMERCHANTIDField;

        

        private object gSTDUTYHEADField;

        private object gSTAPPROPRIATETOField;

        private object gSTTYPEOFSUPPLYField;

        private object gSTNATUREOFSUPPLYField;

        private object cESSVALUATIONMETHODField;

       

        private object nOTIFICATIONSLNOField;

        private object sERVICETAXAPPLICABLEField;

        

        private object eXCISEIMPORTSREGISTARTIONNOField;

        private object eXCISEAPPLICABILITYField;

        private object eXCISETYPEOFBRANCHField;

        private object eXCISEDEFAULTREMOVALField;

        private object eXCISENOTIFICATIONSLNOField;

        private object tYPEOFTARIFFField;

         

        private object tDSDEDUCTEETYPEMSTField;

        
        private object pANSTATUSField;

        private object dEDUCTEEREFERENCEField;

        

        private object iTEXEMPTAPPLICABLEField;

        private object tAXIDENTIFICATIONNOField;

         

        private object bANKACCHOLDERNAMEField;

        private object uSEFORPOSTYPEField;

        private object pAYMENTGATEWAYField;

        private object tYPEOFINTERESTONField;

        private object bANKCONFIGIFSCField;

        private object bANKCONFIGMICRField;

        private object bANKCONFIGSHORTCODEField;

        private object pYMTINSTOUTPUTNAMEField;

        private object pRODUCTCODETYPEField;

        private object sALARYPYMTPRODUCTCODEField;

        private object oTHERPYMTPRODUCTCODEField;

        private object pAYMENTINSTLOCATIONField;

        private object eNCRPTIONLOCATIONField;

        private object nEWIMFLOCATIONField;

        private object iMPORTEDIMFLOCATIONField;

        private object bANKNEWSTATEMENTSField;

        private object bANKIMPORTEDSTATEMENTSField;

        private object bANKMICRField;

        private object cORPORATEUSERNOECSField;

        private object cORPORATEUSERNOACHField;

        private object cORPORATEUSERNAMEField;

        private object iMFNAMEField;

        private object pAYINSBATCHNAMEField;

        private object lASTUSEDBATCHNAMEField;

        private object pAYINSFILENUMPERIODField;

        private object eNCRYPTEDBYField;

        private object eNCRYPTEDIDField;

        private object iSOCURRENCYCODEField;

        private object bANKCAPSULEIDField;

        private object sALESTAXCESSAPPLICABLEField;

        private object bANKIBANField;

        private object vATTAXEXEMPTIONNATUREField;

        private object vATTAXEXEMPTIONNUMBERField;

        private string lEDSTATENAMEField;

        private object vATAPPLICABLEField;

        private object pARTYBUSINESSTYPEField;

        private object pARTYBUSINESSSTYLEField;

        

        private string iSBENEFICIARYCODEONField;

        private string iSUPDATINGTARGETIDField;

        private string aSORIGINALField;

         

        private string iSRATEINCLUSIVEVATField;

         

        private string iSCREDITDAYSCHKONField;

        

        private string iSINTERESTINCLLASTDAYField;

        private string aPPROPRIATETAXVALUEField;

        private string iSBEHAVEASDUTYField;

        private string iNTERESTINCLDAYOFADDITIONField;

        private string iNTERESTINCLDAYOFDEDUCTIONField;

        private string iSOTHTERRITORYASSESSEEField;

        private string oVERRIDECREDITLIMITField;

        private string iSAGAINSTFORMCField;

        private string iSCHEQUEPRINTINGENABLEDField;

        private string iSPAYUPLOADField;

        private string iSPAYBATCHONLYSALField;

        private string iSBNFCODESUPPORTEDField;

        private string aLLOWEXPORTWITHERRORSField;

        private string cONSIDERPURCHASEFOREXPORTField;

        private string iSTRANSPORTERField;

        private string uSEFORNOTIONALITCField;

        private string iSECOMMOPERATORField;
 
        private string iSUSEDFORCVDField;

        private string lEDBELONGSTONONTAXABLEField;

        private string iSEXCISEMERCHANTEXPORTERField;

        private string iSPARTYEXEMPTEDField;

        private string iSSEZPARTYField;

        

        private string iSECHEQUESUPPORTEDField;

        private string iSEDDSUPPORTEDField;

        private string hASECHEQUEDELIVERYMODEField;

        private string hASECHEQUEDELIVERYTOField;

        private string hASECHEQUEPRINTLOCATIONField;

        private string hASECHEQUEPAYABLELOCATIONField;

        private string hASECHEQUEBANKLOCATIONField;

        private string hASEDDDELIVERYMODEField;

        private string hASEDDDELIVERYTOField;

        private string hASEDDPRINTLOCATIONField;

        private string hASEDDPAYABLELOCATIONField;

        private string hASEDDBANKLOCATIONField;

        private string iSEBANKINGENABLEDField;

        private string iSEXPORTFILEENCRYPTEDField;

        private string iSBATCHENABLEDField;

        private string iSPRODUCTCODEBASEDField;

        private string hASEDDCITYField;

        private string hASECHEQUECITYField;

        private string iSFILENAMEFORMATSUPPORTEDField;

        private string hASCLIENTCODEField;

        private string pAYINSISBATCHAPPLICABLEField;

        private string pAYINSISFILENUMAPPField;

        private string iSSALARYTRANSGROUPEDFORBRSField;

        private string iSEBANKINGSUPPORTEDField;

        private string iSSCBUAEField;

        private string iSBANKSTATUSAPPField;

        private string iSSALARYGROUPEDField;

        private string uSEFORPURCHASETAXField;

         

        private uint aLTERIDField;

       

        private byte bENEFICIARYCODEMAXLENGTHField;

        private byte eCHEQUEPRINTLOCATIONVERSIONField;

        private byte eCHEQUEPAYABLELOCATIONVERSIONField;

        private byte eDDPRINTLOCATIONVERSIONField;

        private byte eDDPAYABLELOCATIONVERSIONField;

        private byte pAYINSRUNNINGFILENUMField;

        private byte tRANSACTIONTYPEVERSIONField;

        private byte pAYINSFILENUMLENGTHField;

        
        private byte tEMPGSTCGSTRATEField;

        private byte tEMPGSTSGSTRATEField;

        private byte tEMPGSTIGSTRATEField;

        private object tEMPISVATFIELDSEDITEDField;

        private object tEMPAPPLDATEField;

        private object tEMPCLASSIFICATIONField;

        private object tEMPNATUREField;

        private object tEMPPARTYENTITYField;

        private object tEMPBUSINESSNATUREField;

        private byte tEMPVATRATEField;

        private byte tEMPADDLTAXField;

        private byte tEMPCESSONVATField;

        private object tEMPTAXTYPEField;

        private object tEMPMAJORCOMMODITYNAMEField;

        private object tEMPCOMMODITYNAMEField;

        private object tEMPCOMMODITYCODEField;

        private object tEMPSUBCOMMODITYCODEField;

        private object tEMPUOMField;

        private object tEMPTYPEOFGOODSField;

        private object tEMPTRADENAMEField;

        private object tEMPGOODSNATUREField;

        private object tEMPSCHEDULEField;

        private object tEMPSCHEDULESLNOField;

        private object tEMPISINVDETAILSENABLEField;

        private byte tEMPLOCALVATRATEField;

        private object tEMPVALUATIONTYPEField;

        private object tEMPISCALCONQTYField;

        private object tEMPISSALETOLOCALCITIZENField;

        private object lEDISTDSAPPLICABLECURRLIABField;

        private object iSPRODUCTCODEEDITEDField;

        private object sERVICETAXDETAILSLISTField;

        private object lBTREGNDETAILSLISTField;

        private object vATDETAILSLISTField;

        private object sALESTAXCESSDETAILSLISTField;

        private object gSTDETAILSLISTField;

         

        private object eXCISETARIFFDETAILSLISTField;

        private object tCSCATEGORYDETAILSLISTField;

        private object tDSCATEGORYDETAILSLISTField;

        
        private object eXCISEJURISDICTIONDETAILSLISTField;

        private object eXCLUDEDTAXATIONSLISTField;

         

        private object cANCELLEDPAYALLOCATIONSLISTField;

        private object eCHEQUEPRINTLOCATIONLISTField;

        private object eCHEQUEPAYABLELOCATIONLISTField;

        private object eDDPRINTLOCATIONLISTField;

        private object eDDPAYABLELOCATIONLISTField;

        private object aVAILABLETRANSACTIONTYPESLISTField;

        private object lEDPAYINSCONFIGSLISTField;

        private object tYPECODEDETAILSLISTField;

        private object fIELDVALIDATIONDETAILSLISTField;

        private object iNPUTCRALLOCSLISTField;

        private object gSTCLASSFNIGSTRATESLISTField;

        private object eXTARIFFDUTYHEADDETAILSLISTField;

        private object vOUCHERTYPEPRODUCTCODESLISTField;

        private SISCUSTOMERDETSUDFLIST sISCUSTOMERDETSUDFLISTField;

        private MONTHSRVCAMTUDFLIST mONTHSRVCAMTUDFLISTField;

        private SISSUBSVALIDFROMLIST sISSUBSVALIDFROMLISTField;

        private SISSUBSVALIDUPTOLIST sISSUBSVALIDUPTOLISTField;

        private SISKITTYPEUDFLIST sISKITTYPEUDFLISTField;

        private SISBILLTYPELIST sISBILLTYPELISTField;

        private SISSRVCTYPELIST sISSRVCTYPELISTField;

        private MONTHSRVCITEMUDFLIST mONTHSRVCITEMUDFLISTField;

        

        

       

        /// <remarks/>
        public object PANAPPLICABLEFROM
        {
            get
            {
                return this.pANAPPLICABLEFROMField;
            }
            set
            {
                this.pANAPPLICABLEFROMField = value;
            }
        }

        /// <remarks/>
        public object PAYINSRUNNINGFILEDATE
        {
            get
            {
                return this.pAYINSRUNNINGFILEDATEField;
            }
            set
            {
                this.pAYINSRUNNINGFILEDATEField = value;
            }
        }

        /// <remarks/>
        public object VATTAXEXEMPTIONDATE
        {
            get
            {
                return this.vATTAXEXEMPTIONDATEField;
            }
            set
            {
                this.vATTAXEXEMPTIONDATEField = value;
            }
        }

        /// <remarks/>
        public string GUID
        {
            get
            {
                return this.gUIDField;
            }
            set
            {
                this.gUIDField = value;
            }
        }

 

        /// <remarks/>
        public object WEBSITE
        {
            get
            {
                return this.wEBSITEField;
            }
            set
            {
                this.wEBSITEField = value;
            }
        }
 
        /// <remarks/>
        public string COUNTRYNAME
        {
            get
            {
                return this.cOUNTRYNAMEField;
            }
            set
            {
                this.cOUNTRYNAMEField = value;
            }
        }

   

        /// <remarks/>
        public object LBTREGNNO
        {
            get
            {
                return this.lBTREGNNOField;
            }
            set
            {
                this.lBTREGNNOField = value;
            }
        }

        /// <remarks/>
        public object LBTZONE
        {
            get
            {
                return this.lBTZONEField;
            }
            set
            {
                this.lBTZONEField = value;
            }
        }

        /// <remarks/>
        public object EXPORTIMPORTCODE
        {
            get
            {
                return this.eXPORTIMPORTCODEField;
            }
            set
            {
                this.eXPORTIMPORTCODEField = value;
            }
        }

        /// <remarks/>
        public string GSTREGISTRATIONTYPE
        {
            get
            {
                return this.gSTREGISTRATIONTYPEField;
            }
            set
            {
                this.gSTREGISTRATIONTYPEField = value;
            }
        }

 

        /// <remarks/>
        public object UPLOADLASTREFRESH
        {
            get
            {
                return this.uPLOADLASTREFRESHField;
            }
            set
            {
                this.uPLOADLASTREFRESHField = value;
            }
        }

 

        /// <remarks/>
        public object REQUESTORRULE
        {
            get
            {
                return this.rEQUESTORRULEField;
            }
            set
            {
                this.rEQUESTORRULEField = value;
            }
        }

 

        /// <remarks/>
        public object TDSAPPLICABLE
        {
            get
            {
                return this.tDSAPPLICABLEField;
            }
            set
            {
                this.tDSAPPLICABLEField = value;
            }
        }

        /// <remarks/>
        public object TCSAPPLICABLE
        {
            get
            {
                return this.tCSAPPLICABLEField;
            }
            set
            {
                this.tCSAPPLICABLEField = value;
            }
        }

        /// <remarks/>
        public object GSTAPPLICABLE
        {
            get
            {
                return this.gSTAPPLICABLEField;
            }
            set
            {
                this.gSTAPPLICABLEField = value;
            }
        }

     

  

        /// <remarks/>
        public object DESCRIPTION
        {
            get
            {
                return this.dESCRIPTIONField;
            }
            set
            {
                this.dESCRIPTIONField = value;
            }
        }

        /// <remarks/>
        public object LEDADDLALLOCTYPE
        {
            get
            {
                return this.lEDADDLALLOCTYPEField;
            }
            set
            {
                this.lEDADDLALLOCTYPEField = value;
            }
        }

        /// <remarks/>
        public object TRANSPORTERID
        {
            get
            {
                return this.tRANSPORTERIDField;
            }
            set
            {
                this.tRANSPORTERIDField = value;
            }
        }

       

        /// <remarks/>
        public object MAILINGNAMENATIVE
        {
            get
            {
                return this.mAILINGNAMENATIVEField;
            }
            set
            {
                this.mAILINGNAMENATIVEField = value;
            }
        }

        /// <remarks/>
        public object STATENAMENATIVE
        {
            get
            {
                return this.sTATENAMENATIVEField;
            }
            set
            {
                this.sTATENAMENATIVEField = value;
            }
        }

        /// <remarks/>
        public object COUNTRYNAMENATIVE
        {
            get
            {
                return this.cOUNTRYNAMENATIVEField;
            }
            set
            {
                this.cOUNTRYNAMENATIVEField = value;
            }
        }

  

        /// <remarks/>
        public object NAMEONPAN
        {
            get
            {
                return this.nAMEONPANField;
            }
            set
            {
                this.nAMEONPANField = value;
            }
        }

        /// <remarks/>
        public object USEDFORTAXTYPE
        {
            get
            {
                return this.uSEDFORTAXTYPEField;
            }
            set
            {
                this.uSEDFORTAXTYPEField = value;
            }
        }

        /// <remarks/>
        public object ECOMMMERCHANTID
        {
            get
            {
                return this.eCOMMMERCHANTIDField;
            }
            set
            {
                this.eCOMMMERCHANTIDField = value;
            }
        }

        /// <remarks/>
        public string PARTYGSTIN
        {
            get
            {
                return this.pARTYGSTINField;
            }
            set
            {
                this.pARTYGSTINField = value;
            }
        }

        /// <remarks/>
        public object GSTDUTYHEAD
        {
            get
            {
                return this.gSTDUTYHEADField;
            }
            set
            {
                this.gSTDUTYHEADField = value;
            }
        }

        /// <remarks/>
        public object GSTAPPROPRIATETO
        {
            get
            {
                return this.gSTAPPROPRIATETOField;
            }
            set
            {
                this.gSTAPPROPRIATETOField = value;
            }
        }

        /// <remarks/>
        public object GSTTYPEOFSUPPLY
        {
            get
            {
                return this.gSTTYPEOFSUPPLYField;
            }
            set
            {
                this.gSTTYPEOFSUPPLYField = value;
            }
        }

        /// <remarks/>
        public object GSTNATUREOFSUPPLY
        {
            get
            {
                return this.gSTNATUREOFSUPPLYField;
            }
            set
            {
                this.gSTNATUREOFSUPPLYField = value;
            }
        }

        /// <remarks/>
        public object CESSVALUATIONMETHOD
        {
            get
            {
                return this.cESSVALUATIONMETHODField;
            }
            set
            {
                this.cESSVALUATIONMETHODField = value;
            }
        }
 

        /// <remarks/>
        public object NOTIFICATIONSLNO
        {
            get
            {
                return this.nOTIFICATIONSLNOField;
            }
            set
            {
                this.nOTIFICATIONSLNOField = value;
            }
        }

        /// <remarks/>
        public object SERVICETAXAPPLICABLE
        {
            get
            {
                return this.sERVICETAXAPPLICABLEField;
            }
            set
            {
                this.sERVICETAXAPPLICABLEField = value;
            }
        }

      

        /// <remarks/>
        public object EXCISEIMPORTSREGISTARTIONNO
        {
            get
            {
                return this.eXCISEIMPORTSREGISTARTIONNOField;
            }
            set
            {
                this.eXCISEIMPORTSREGISTARTIONNOField = value;
            }
        }

        /// <remarks/>
        public object EXCISEAPPLICABILITY
        {
            get
            {
                return this.eXCISEAPPLICABILITYField;
            }
            set
            {
                this.eXCISEAPPLICABILITYField = value;
            }
        }

        /// <remarks/>
        public object EXCISETYPEOFBRANCH
        {
            get
            {
                return this.eXCISETYPEOFBRANCHField;
            }
            set
            {
                this.eXCISETYPEOFBRANCHField = value;
            }
        }

        /// <remarks/>
        public object EXCISEDEFAULTREMOVAL
        {
            get
            {
                return this.eXCISEDEFAULTREMOVALField;
            }
            set
            {
                this.eXCISEDEFAULTREMOVALField = value;
            }
        }

        /// <remarks/>
        public object EXCISENOTIFICATIONSLNO
        {
            get
            {
                return this.eXCISENOTIFICATIONSLNOField;
            }
            set
            {
                this.eXCISENOTIFICATIONSLNOField = value;
            }
        }

        /// <remarks/>
        public object TYPEOFTARIFF
        {
            get
            {
                return this.tYPEOFTARIFFField;
            }
            set
            {
                this.tYPEOFTARIFFField = value;
            }
        }
 

        /// <remarks/>
        public object TDSDEDUCTEETYPEMST
        {
            get
            {
                return this.tDSDEDUCTEETYPEMSTField;
            }
            set
            {
                this.tDSDEDUCTEETYPEMSTField = value;
            }
        }

      
        /// <remarks/>
        public object PANSTATUS
        {
            get
            {
                return this.pANSTATUSField;
            }
            set
            {
                this.pANSTATUSField = value;
            }
        }

        /// <remarks/>
        public object DEDUCTEEREFERENCE
        {
            get
            {
                return this.dEDUCTEEREFERENCEField;
            }
            set
            {
                this.dEDUCTEEREFERENCEField = value;
            }
        }

 

        /// <remarks/>
        public object ITEXEMPTAPPLICABLE
        {
            get
            {
                return this.iTEXEMPTAPPLICABLEField;
            }
            set
            {
                this.iTEXEMPTAPPLICABLEField = value;
            }
        }

        /// <remarks/>
        public object TAXIDENTIFICATIONNO
        {
            get
            {
                return this.tAXIDENTIFICATIONNOField;
            }
            set
            {
                this.tAXIDENTIFICATIONNOField = value;
            }
        }

       

        
 
        /// <remarks/>
        public object BANKACCHOLDERNAME
        {
            get
            {
                return this.bANKACCHOLDERNAMEField;
            }
            set
            {
                this.bANKACCHOLDERNAMEField = value;
            }
        }

        /// <remarks/>
        public object USEFORPOSTYPE
        {
            get
            {
                return this.uSEFORPOSTYPEField;
            }
            set
            {
                this.uSEFORPOSTYPEField = value;
            }
        }

        /// <remarks/>
        public object PAYMENTGATEWAY
        {
            get
            {
                return this.pAYMENTGATEWAYField;
            }
            set
            {
                this.pAYMENTGATEWAYField = value;
            }
        }

        /// <remarks/>
        public object TYPEOFINTERESTON
        {
            get
            {
                return this.tYPEOFINTERESTONField;
            }
            set
            {
                this.tYPEOFINTERESTONField = value;
            }
        }

        /// <remarks/>
        public object BANKCONFIGIFSC
        {
            get
            {
                return this.bANKCONFIGIFSCField;
            }
            set
            {
                this.bANKCONFIGIFSCField = value;
            }
        }

        /// <remarks/>
        public object BANKCONFIGMICR
        {
            get
            {
                return this.bANKCONFIGMICRField;
            }
            set
            {
                this.bANKCONFIGMICRField = value;
            }
        }

        /// <remarks/>
        public object BANKCONFIGSHORTCODE
        {
            get
            {
                return this.bANKCONFIGSHORTCODEField;
            }
            set
            {
                this.bANKCONFIGSHORTCODEField = value;
            }
        }

        /// <remarks/>
        public object PYMTINSTOUTPUTNAME
        {
            get
            {
                return this.pYMTINSTOUTPUTNAMEField;
            }
            set
            {
                this.pYMTINSTOUTPUTNAMEField = value;
            }
        }

        /// <remarks/>
        public object PRODUCTCODETYPE
        {
            get
            {
                return this.pRODUCTCODETYPEField;
            }
            set
            {
                this.pRODUCTCODETYPEField = value;
            }
        }

        /// <remarks/>
        public object SALARYPYMTPRODUCTCODE
        {
            get
            {
                return this.sALARYPYMTPRODUCTCODEField;
            }
            set
            {
                this.sALARYPYMTPRODUCTCODEField = value;
            }
        }

        /// <remarks/>
        public object OTHERPYMTPRODUCTCODE
        {
            get
            {
                return this.oTHERPYMTPRODUCTCODEField;
            }
            set
            {
                this.oTHERPYMTPRODUCTCODEField = value;
            }
        }

        /// <remarks/>
        public object PAYMENTINSTLOCATION
        {
            get
            {
                return this.pAYMENTINSTLOCATIONField;
            }
            set
            {
                this.pAYMENTINSTLOCATIONField = value;
            }
        }

        /// <remarks/>
        public object ENCRPTIONLOCATION
        {
            get
            {
                return this.eNCRPTIONLOCATIONField;
            }
            set
            {
                this.eNCRPTIONLOCATIONField = value;
            }
        }

        /// <remarks/>
        public object NEWIMFLOCATION
        {
            get
            {
                return this.nEWIMFLOCATIONField;
            }
            set
            {
                this.nEWIMFLOCATIONField = value;
            }
        }

        /// <remarks/>
        public object IMPORTEDIMFLOCATION
        {
            get
            {
                return this.iMPORTEDIMFLOCATIONField;
            }
            set
            {
                this.iMPORTEDIMFLOCATIONField = value;
            }
        }

        /// <remarks/>
        public object BANKNEWSTATEMENTS
        {
            get
            {
                return this.bANKNEWSTATEMENTSField;
            }
            set
            {
                this.bANKNEWSTATEMENTSField = value;
            }
        }

        /// <remarks/>
        public object BANKIMPORTEDSTATEMENTS
        {
            get
            {
                return this.bANKIMPORTEDSTATEMENTSField;
            }
            set
            {
                this.bANKIMPORTEDSTATEMENTSField = value;
            }
        }

        /// <remarks/>
        public object BANKMICR
        {
            get
            {
                return this.bANKMICRField;
            }
            set
            {
                this.bANKMICRField = value;
            }
        }

        /// <remarks/>
        public object CORPORATEUSERNOECS
        {
            get
            {
                return this.cORPORATEUSERNOECSField;
            }
            set
            {
                this.cORPORATEUSERNOECSField = value;
            }
        }

        /// <remarks/>
        public object CORPORATEUSERNOACH
        {
            get
            {
                return this.cORPORATEUSERNOACHField;
            }
            set
            {
                this.cORPORATEUSERNOACHField = value;
            }
        }

        /// <remarks/>
        public object CORPORATEUSERNAME
        {
            get
            {
                return this.cORPORATEUSERNAMEField;
            }
            set
            {
                this.cORPORATEUSERNAMEField = value;
            }
        }

        /// <remarks/>
        public object IMFNAME
        {
            get
            {
                return this.iMFNAMEField;
            }
            set
            {
                this.iMFNAMEField = value;
            }
        }

        /// <remarks/>
        public object PAYINSBATCHNAME
        {
            get
            {
                return this.pAYINSBATCHNAMEField;
            }
            set
            {
                this.pAYINSBATCHNAMEField = value;
            }
        }

        /// <remarks/>
        public object LASTUSEDBATCHNAME
        {
            get
            {
                return this.lASTUSEDBATCHNAMEField;
            }
            set
            {
                this.lASTUSEDBATCHNAMEField = value;
            }
        }

        /// <remarks/>
        public object PAYINSFILENUMPERIOD
        {
            get
            {
                return this.pAYINSFILENUMPERIODField;
            }
            set
            {
                this.pAYINSFILENUMPERIODField = value;
            }
        }

        /// <remarks/>
        public object ENCRYPTEDBY
        {
            get
            {
                return this.eNCRYPTEDBYField;
            }
            set
            {
                this.eNCRYPTEDBYField = value;
            }
        }

        /// <remarks/>
        public object ENCRYPTEDID
        {
            get
            {
                return this.eNCRYPTEDIDField;
            }
            set
            {
                this.eNCRYPTEDIDField = value;
            }
        }

        /// <remarks/>
        public object ISOCURRENCYCODE
        {
            get
            {
                return this.iSOCURRENCYCODEField;
            }
            set
            {
                this.iSOCURRENCYCODEField = value;
            }
        }

        /// <remarks/>
        public object BANKCAPSULEID
        {
            get
            {
                return this.bANKCAPSULEIDField;
            }
            set
            {
                this.bANKCAPSULEIDField = value;
            }
        }

        /// <remarks/>
        public object SALESTAXCESSAPPLICABLE
        {
            get
            {
                return this.sALESTAXCESSAPPLICABLEField;
            }
            set
            {
                this.sALESTAXCESSAPPLICABLEField = value;
            }
        }

        /// <remarks/>
        public object BANKIBAN
        {
            get
            {
                return this.bANKIBANField;
            }
            set
            {
                this.bANKIBANField = value;
            }
        }

        /// <remarks/>
        public object VATTAXEXEMPTIONNATURE
        {
            get
            {
                return this.vATTAXEXEMPTIONNATUREField;
            }
            set
            {
                this.vATTAXEXEMPTIONNATUREField = value;
            }
        }

        /// <remarks/>
        public object VATTAXEXEMPTIONNUMBER
        {
            get
            {
                return this.vATTAXEXEMPTIONNUMBERField;
            }
            set
            {
                this.vATTAXEXEMPTIONNUMBERField = value;
            }
        }

        /// <remarks/>
        public string LEDSTATENAME
        {
            get
            {
                return this.lEDSTATENAMEField;
            }
            set
            {
                this.lEDSTATENAMEField = value;
            }
        }

        /// <remarks/>
        public object VATAPPLICABLE
        {
            get
            {
                return this.vATAPPLICABLEField;
            }
            set
            {
                this.vATAPPLICABLEField = value;
            }
        }

        /// <remarks/>
        public object PARTYBUSINESSTYPE
        {
            get
            {
                return this.pARTYBUSINESSTYPEField;
            }
            set
            {
                this.pARTYBUSINESSTYPEField = value;
            }
        }

        /// <remarks/>
        public object PARTYBUSINESSSTYLE
        {
            get
            {
                return this.pARTYBUSINESSSTYLEField;
            }
            set
            {
                this.pARTYBUSINESSSTYLEField = value;
            }
        } 

        /// <remarks/>
        public string ISBENEFICIARYCODEON
        {
            get
            {
                return this.iSBENEFICIARYCODEONField;
            }
            set
            {
                this.iSBENEFICIARYCODEONField = value;
            }
        }

        /// <remarks/>
        public string ISUPDATINGTARGETID
        {
            get
            {
                return this.iSUPDATINGTARGETIDField;
            }
            set
            {
                this.iSUPDATINGTARGETIDField = value;
            }
        }

        /// <remarks/>
        public string ASORIGINAL
        {
            get
            {
                return this.aSORIGINALField;
            }
            set
            {
                this.aSORIGINALField = value;
            }
        }

     

        /// <remarks/>
        public string ISRATEINCLUSIVEVAT
        {
            get
            {
                return this.iSRATEINCLUSIVEVATField;
            }
            set
            {
                this.iSRATEINCLUSIVEVATField = value;
            }
        }

  

        /// <remarks/>
        public string ISCREDITDAYSCHKON
        {
            get
            {
                return this.iSCREDITDAYSCHKONField;
            }
            set
            {
                this.iSCREDITDAYSCHKONField = value;
            }
        }

        

        /// <remarks/>
        public string ISINTERESTINCLLASTDAY
        {
            get
            {
                return this.iSINTERESTINCLLASTDAYField;
            }
            set
            {
                this.iSINTERESTINCLLASTDAYField = value;
            }
        }

        /// <remarks/>
        public string APPROPRIATETAXVALUE
        {
            get
            {
                return this.aPPROPRIATETAXVALUEField;
            }
            set
            {
                this.aPPROPRIATETAXVALUEField = value;
            }
        }

        /// <remarks/>
        public string ISBEHAVEASDUTY
        {
            get
            {
                return this.iSBEHAVEASDUTYField;
            }
            set
            {
                this.iSBEHAVEASDUTYField = value;
            }
        }

        /// <remarks/>
        public string INTERESTINCLDAYOFADDITION
        {
            get
            {
                return this.iNTERESTINCLDAYOFADDITIONField;
            }
            set
            {
                this.iNTERESTINCLDAYOFADDITIONField = value;
            }
        }

        /// <remarks/>
        public string INTERESTINCLDAYOFDEDUCTION
        {
            get
            {
                return this.iNTERESTINCLDAYOFDEDUCTIONField;
            }
            set
            {
                this.iNTERESTINCLDAYOFDEDUCTIONField = value;
            }
        }

        /// <remarks/>
        public string ISOTHTERRITORYASSESSEE
        {
            get
            {
                return this.iSOTHTERRITORYASSESSEEField;
            }
            set
            {
                this.iSOTHTERRITORYASSESSEEField = value;
            }
        }

        /// <remarks/>
        public string OVERRIDECREDITLIMIT
        {
            get
            {
                return this.oVERRIDECREDITLIMITField;
            }
            set
            {
                this.oVERRIDECREDITLIMITField = value;
            }
        }

        /// <remarks/>
        public string ISAGAINSTFORMC
        {
            get
            {
                return this.iSAGAINSTFORMCField;
            }
            set
            {
                this.iSAGAINSTFORMCField = value;
            }
        }

        /// <remarks/>
        public string ISCHEQUEPRINTINGENABLED
        {
            get
            {
                return this.iSCHEQUEPRINTINGENABLEDField;
            }
            set
            {
                this.iSCHEQUEPRINTINGENABLEDField = value;
            }
        }

        /// <remarks/>
        public string ISPAYUPLOAD
        {
            get
            {
                return this.iSPAYUPLOADField;
            }
            set
            {
                this.iSPAYUPLOADField = value;
            }
        }

        /// <remarks/>
        public string ISPAYBATCHONLYSAL
        {
            get
            {
                return this.iSPAYBATCHONLYSALField;
            }
            set
            {
                this.iSPAYBATCHONLYSALField = value;
            }
        }

        /// <remarks/>
        public string ISBNFCODESUPPORTED
        {
            get
            {
                return this.iSBNFCODESUPPORTEDField;
            }
            set
            {
                this.iSBNFCODESUPPORTEDField = value;
            }
        }

        /// <remarks/>
        public string ALLOWEXPORTWITHERRORS
        {
            get
            {
                return this.aLLOWEXPORTWITHERRORSField;
            }
            set
            {
                this.aLLOWEXPORTWITHERRORSField = value;
            }
        }

        /// <remarks/>
        public string CONSIDERPURCHASEFOREXPORT
        {
            get
            {
                return this.cONSIDERPURCHASEFOREXPORTField;
            }
            set
            {
                this.cONSIDERPURCHASEFOREXPORTField = value;
            }
        }

        /// <remarks/>
        public string ISTRANSPORTER
        {
            get
            {
                return this.iSTRANSPORTERField;
            }
            set
            {
                this.iSTRANSPORTERField = value;
            }
        }

        /// <remarks/>
        public string USEFORNOTIONALITC
        {
            get
            {
                return this.uSEFORNOTIONALITCField;
            }
            set
            {
                this.uSEFORNOTIONALITCField = value;
            }
        }

        /// <remarks/>
        public string ISECOMMOPERATOR
        {
            get
            {
                return this.iSECOMMOPERATORField;
            }
            set
            {
                this.iSECOMMOPERATORField = value;
            }
        }
 
        /// <remarks/>
        public string ISUSEDFORCVD
        {
            get
            {
                return this.iSUSEDFORCVDField;
            }
            set
            {
                this.iSUSEDFORCVDField = value;
            }
        }

        /// <remarks/>
        public string LEDBELONGSTONONTAXABLE
        {
            get
            {
                return this.lEDBELONGSTONONTAXABLEField;
            }
            set
            {
                this.lEDBELONGSTONONTAXABLEField = value;
            }
        }

        /// <remarks/>
        public string ISEXCISEMERCHANTEXPORTER
        {
            get
            {
                return this.iSEXCISEMERCHANTEXPORTERField;
            }
            set
            {
                this.iSEXCISEMERCHANTEXPORTERField = value;
            }
        }

        /// <remarks/>
        public string ISPARTYEXEMPTED
        {
            get
            {
                return this.iSPARTYEXEMPTEDField;
            }
            set
            {
                this.iSPARTYEXEMPTEDField = value;
            }
        }

        /// <remarks/>
        public string ISSEZPARTY
        {
            get
            {
                return this.iSSEZPARTYField;
            }
            set
            {
                this.iSSEZPARTYField = value;
            }
        }

        

        /// <remarks/>
        public string ISECHEQUESUPPORTED
        {
            get
            {
                return this.iSECHEQUESUPPORTEDField;
            }
            set
            {
                this.iSECHEQUESUPPORTEDField = value;
            }
        }

        /// <remarks/>
        public string ISEDDSUPPORTED
        {
            get
            {
                return this.iSEDDSUPPORTEDField;
            }
            set
            {
                this.iSEDDSUPPORTEDField = value;
            }
        }

        /// <remarks/>
        public string HASECHEQUEDELIVERYMODE
        {
            get
            {
                return this.hASECHEQUEDELIVERYMODEField;
            }
            set
            {
                this.hASECHEQUEDELIVERYMODEField = value;
            }
        }

        /// <remarks/>
        public string HASECHEQUEDELIVERYTO
        {
            get
            {
                return this.hASECHEQUEDELIVERYTOField;
            }
            set
            {
                this.hASECHEQUEDELIVERYTOField = value;
            }
        }

        /// <remarks/>
        public string HASECHEQUEPRINTLOCATION
        {
            get
            {
                return this.hASECHEQUEPRINTLOCATIONField;
            }
            set
            {
                this.hASECHEQUEPRINTLOCATIONField = value;
            }
        }

        /// <remarks/>
        public string HASECHEQUEPAYABLELOCATION
        {
            get
            {
                return this.hASECHEQUEPAYABLELOCATIONField;
            }
            set
            {
                this.hASECHEQUEPAYABLELOCATIONField = value;
            }
        }

        /// <remarks/>
        public string HASECHEQUEBANKLOCATION
        {
            get
            {
                return this.hASECHEQUEBANKLOCATIONField;
            }
            set
            {
                this.hASECHEQUEBANKLOCATIONField = value;
            }
        }

        /// <remarks/>
        public string HASEDDDELIVERYMODE
        {
            get
            {
                return this.hASEDDDELIVERYMODEField;
            }
            set
            {
                this.hASEDDDELIVERYMODEField = value;
            }
        }

        /// <remarks/>
        public string HASEDDDELIVERYTO
        {
            get
            {
                return this.hASEDDDELIVERYTOField;
            }
            set
            {
                this.hASEDDDELIVERYTOField = value;
            }
        }

        /// <remarks/>
        public string HASEDDPRINTLOCATION
        {
            get
            {
                return this.hASEDDPRINTLOCATIONField;
            }
            set
            {
                this.hASEDDPRINTLOCATIONField = value;
            }
        }

        /// <remarks/>
        public string HASEDDPAYABLELOCATION
        {
            get
            {
                return this.hASEDDPAYABLELOCATIONField;
            }
            set
            {
                this.hASEDDPAYABLELOCATIONField = value;
            }
        }

        /// <remarks/>
        public string HASEDDBANKLOCATION
        {
            get
            {
                return this.hASEDDBANKLOCATIONField;
            }
            set
            {
                this.hASEDDBANKLOCATIONField = value;
            }
        }

        /// <remarks/>
        public string ISEBANKINGENABLED
        {
            get
            {
                return this.iSEBANKINGENABLEDField;
            }
            set
            {
                this.iSEBANKINGENABLEDField = value;
            }
        }

        /// <remarks/>
        public string ISEXPORTFILEENCRYPTED
        {
            get
            {
                return this.iSEXPORTFILEENCRYPTEDField;
            }
            set
            {
                this.iSEXPORTFILEENCRYPTEDField = value;
            }
        }

        /// <remarks/>
        public string ISBATCHENABLED
        {
            get
            {
                return this.iSBATCHENABLEDField;
            }
            set
            {
                this.iSBATCHENABLEDField = value;
            }
        }

        /// <remarks/>
        public string ISPRODUCTCODEBASED
        {
            get
            {
                return this.iSPRODUCTCODEBASEDField;
            }
            set
            {
                this.iSPRODUCTCODEBASEDField = value;
            }
        }

        /// <remarks/>
        public string HASEDDCITY
        {
            get
            {
                return this.hASEDDCITYField;
            }
            set
            {
                this.hASEDDCITYField = value;
            }
        }

        /// <remarks/>
        public string HASECHEQUECITY
        {
            get
            {
                return this.hASECHEQUECITYField;
            }
            set
            {
                this.hASECHEQUECITYField = value;
            }
        }

        /// <remarks/>
        public string ISFILENAMEFORMATSUPPORTED
        {
            get
            {
                return this.iSFILENAMEFORMATSUPPORTEDField;
            }
            set
            {
                this.iSFILENAMEFORMATSUPPORTEDField = value;
            }
        }

        /// <remarks/>
        public string HASCLIENTCODE
        {
            get
            {
                return this.hASCLIENTCODEField;
            }
            set
            {
                this.hASCLIENTCODEField = value;
            }
        }

        /// <remarks/>
        public string PAYINSISBATCHAPPLICABLE
        {
            get
            {
                return this.pAYINSISBATCHAPPLICABLEField;
            }
            set
            {
                this.pAYINSISBATCHAPPLICABLEField = value;
            }
        }

        /// <remarks/>
        public string PAYINSISFILENUMAPP
        {
            get
            {
                return this.pAYINSISFILENUMAPPField;
            }
            set
            {
                this.pAYINSISFILENUMAPPField = value;
            }
        }

        /// <remarks/>
        public string ISSALARYTRANSGROUPEDFORBRS
        {
            get
            {
                return this.iSSALARYTRANSGROUPEDFORBRSField;
            }
            set
            {
                this.iSSALARYTRANSGROUPEDFORBRSField = value;
            }
        }

        /// <remarks/>
        public string ISEBANKINGSUPPORTED
        {
            get
            {
                return this.iSEBANKINGSUPPORTEDField;
            }
            set
            {
                this.iSEBANKINGSUPPORTEDField = value;
            }
        }

        /// <remarks/>
        public string ISSCBUAE
        {
            get
            {
                return this.iSSCBUAEField;
            }
            set
            {
                this.iSSCBUAEField = value;
            }
        }

        /// <remarks/>
        public string ISBANKSTATUSAPP
        {
            get
            {
                return this.iSBANKSTATUSAPPField;
            }
            set
            {
                this.iSBANKSTATUSAPPField = value;
            }
        }

        /// <remarks/>
        public string ISSALARYGROUPED
        {
            get
            {
                return this.iSSALARYGROUPEDField;
            }
            set
            {
                this.iSSALARYGROUPEDField = value;
            }
        }

        /// <remarks/>
        public string USEFORPURCHASETAX
        {
            get
            {
                return this.uSEFORPURCHASETAXField;
            }
            set
            {
                this.uSEFORPURCHASETAXField = value;
            }
        }

        

        /// <remarks/>
        public uint ALTERID
        {
            get
            {
                return this.aLTERIDField;
            }
            set
            {
                this.aLTERIDField = value;
            }
        }

        

        /// <remarks/>
        public byte BENEFICIARYCODEMAXLENGTH
        {
            get
            {
                return this.bENEFICIARYCODEMAXLENGTHField;
            }
            set
            {
                this.bENEFICIARYCODEMAXLENGTHField = value;
            }
        }

        /// <remarks/>
        public byte ECHEQUEPRINTLOCATIONVERSION
        {
            get
            {
                return this.eCHEQUEPRINTLOCATIONVERSIONField;
            }
            set
            {
                this.eCHEQUEPRINTLOCATIONVERSIONField = value;
            }
        }

        /// <remarks/>
        public byte ECHEQUEPAYABLELOCATIONVERSION
        {
            get
            {
                return this.eCHEQUEPAYABLELOCATIONVERSIONField;
            }
            set
            {
                this.eCHEQUEPAYABLELOCATIONVERSIONField = value;
            }
        }

        /// <remarks/>
        public byte EDDPRINTLOCATIONVERSION
        {
            get
            {
                return this.eDDPRINTLOCATIONVERSIONField;
            }
            set
            {
                this.eDDPRINTLOCATIONVERSIONField = value;
            }
        }

        /// <remarks/>
        public byte EDDPAYABLELOCATIONVERSION
        {
            get
            {
                return this.eDDPAYABLELOCATIONVERSIONField;
            }
            set
            {
                this.eDDPAYABLELOCATIONVERSIONField = value;
            }
        }

        /// <remarks/>
        public byte PAYINSRUNNINGFILENUM
        {
            get
            {
                return this.pAYINSRUNNINGFILENUMField;
            }
            set
            {
                this.pAYINSRUNNINGFILENUMField = value;
            }
        }

        /// <remarks/>
        public byte TRANSACTIONTYPEVERSION
        {
            get
            {
                return this.tRANSACTIONTYPEVERSIONField;
            }
            set
            {
                this.tRANSACTIONTYPEVERSIONField = value;
            }
        }

        /// <remarks/>
        public byte PAYINSFILENUMLENGTH
        {
            get
            {
                return this.pAYINSFILENUMLENGTHField;
            }
            set
            {
                this.pAYINSFILENUMLENGTHField = value;
            }
        }

        

        /// <remarks/>
        public byte TEMPGSTCGSTRATE
        {
            get
            {
                return this.tEMPGSTCGSTRATEField;
            }
            set
            {
                this.tEMPGSTCGSTRATEField = value;
            }
        }

        /// <remarks/>
        public byte TEMPGSTSGSTRATE
        {
            get
            {
                return this.tEMPGSTSGSTRATEField;
            }
            set
            {
                this.tEMPGSTSGSTRATEField = value;
            }
        }

        /// <remarks/>
        public byte TEMPGSTIGSTRATE
        {
            get
            {
                return this.tEMPGSTIGSTRATEField;
            }
            set
            {
                this.tEMPGSTIGSTRATEField = value;
            }
        }

        /// <remarks/>
        public object TEMPISVATFIELDSEDITED
        {
            get
            {
                return this.tEMPISVATFIELDSEDITEDField;
            }
            set
            {
                this.tEMPISVATFIELDSEDITEDField = value;
            }
        }

        /// <remarks/>
        public object TEMPAPPLDATE
        {
            get
            {
                return this.tEMPAPPLDATEField;
            }
            set
            {
                this.tEMPAPPLDATEField = value;
            }
        }

        /// <remarks/>
        public object TEMPCLASSIFICATION
        {
            get
            {
                return this.tEMPCLASSIFICATIONField;
            }
            set
            {
                this.tEMPCLASSIFICATIONField = value;
            }
        }

        /// <remarks/>
        public object TEMPNATURE
        {
            get
            {
                return this.tEMPNATUREField;
            }
            set
            {
                this.tEMPNATUREField = value;
            }
        }

        /// <remarks/>
        public object TEMPPARTYENTITY
        {
            get
            {
                return this.tEMPPARTYENTITYField;
            }
            set
            {
                this.tEMPPARTYENTITYField = value;
            }
        }

        /// <remarks/>
        public object TEMPBUSINESSNATURE
        {
            get
            {
                return this.tEMPBUSINESSNATUREField;
            }
            set
            {
                this.tEMPBUSINESSNATUREField = value;
            }
        }

        /// <remarks/>
        public byte TEMPVATRATE
        {
            get
            {
                return this.tEMPVATRATEField;
            }
            set
            {
                this.tEMPVATRATEField = value;
            }
        }

        /// <remarks/>
        public byte TEMPADDLTAX
        {
            get
            {
                return this.tEMPADDLTAXField;
            }
            set
            {
                this.tEMPADDLTAXField = value;
            }
        }

        /// <remarks/>
        public byte TEMPCESSONVAT
        {
            get
            {
                return this.tEMPCESSONVATField;
            }
            set
            {
                this.tEMPCESSONVATField = value;
            }
        }

        /// <remarks/>
        public object TEMPTAXTYPE
        {
            get
            {
                return this.tEMPTAXTYPEField;
            }
            set
            {
                this.tEMPTAXTYPEField = value;
            }
        }

        /// <remarks/>
        public object TEMPMAJORCOMMODITYNAME
        {
            get
            {
                return this.tEMPMAJORCOMMODITYNAMEField;
            }
            set
            {
                this.tEMPMAJORCOMMODITYNAMEField = value;
            }
        }

        /// <remarks/>
        public object TEMPCOMMODITYNAME
        {
            get
            {
                return this.tEMPCOMMODITYNAMEField;
            }
            set
            {
                this.tEMPCOMMODITYNAMEField = value;
            }
        }

        /// <remarks/>
        public object TEMPCOMMODITYCODE
        {
            get
            {
                return this.tEMPCOMMODITYCODEField;
            }
            set
            {
                this.tEMPCOMMODITYCODEField = value;
            }
        }

        /// <remarks/>
        public object TEMPSUBCOMMODITYCODE
        {
            get
            {
                return this.tEMPSUBCOMMODITYCODEField;
            }
            set
            {
                this.tEMPSUBCOMMODITYCODEField = value;
            }
        }

        /// <remarks/>
        public object TEMPUOM
        {
            get
            {
                return this.tEMPUOMField;
            }
            set
            {
                this.tEMPUOMField = value;
            }
        }

        /// <remarks/>
        public object TEMPTYPEOFGOODS
        {
            get
            {
                return this.tEMPTYPEOFGOODSField;
            }
            set
            {
                this.tEMPTYPEOFGOODSField = value;
            }
        }

        /// <remarks/>
        public object TEMPTRADENAME
        {
            get
            {
                return this.tEMPTRADENAMEField;
            }
            set
            {
                this.tEMPTRADENAMEField = value;
            }
        }

        /// <remarks/>
        public object TEMPGOODSNATURE
        {
            get
            {
                return this.tEMPGOODSNATUREField;
            }
            set
            {
                this.tEMPGOODSNATUREField = value;
            }
        }

        /// <remarks/>
        public object TEMPSCHEDULE
        {
            get
            {
                return this.tEMPSCHEDULEField;
            }
            set
            {
                this.tEMPSCHEDULEField = value;
            }
        }

        /// <remarks/>
        public object TEMPSCHEDULESLNO
        {
            get
            {
                return this.tEMPSCHEDULESLNOField;
            }
            set
            {
                this.tEMPSCHEDULESLNOField = value;
            }
        }

        /// <remarks/>
        public object TEMPISINVDETAILSENABLE
        {
            get
            {
                return this.tEMPISINVDETAILSENABLEField;
            }
            set
            {
                this.tEMPISINVDETAILSENABLEField = value;
            }
        }

        /// <remarks/>
        public byte TEMPLOCALVATRATE
        {
            get
            {
                return this.tEMPLOCALVATRATEField;
            }
            set
            {
                this.tEMPLOCALVATRATEField = value;
            }
        }

        /// <remarks/>
        public object TEMPVALUATIONTYPE
        {
            get
            {
                return this.tEMPVALUATIONTYPEField;
            }
            set
            {
                this.tEMPVALUATIONTYPEField = value;
            }
        }

        /// <remarks/>
        public object TEMPISCALCONQTY
        {
            get
            {
                return this.tEMPISCALCONQTYField;
            }
            set
            {
                this.tEMPISCALCONQTYField = value;
            }
        }

        /// <remarks/>
        public object TEMPISSALETOLOCALCITIZEN
        {
            get
            {
                return this.tEMPISSALETOLOCALCITIZENField;
            }
            set
            {
                this.tEMPISSALETOLOCALCITIZENField = value;
            }
        }

        /// <remarks/>
        public object LEDISTDSAPPLICABLECURRLIAB
        {
            get
            {
                return this.lEDISTDSAPPLICABLECURRLIABField;
            }
            set
            {
                this.lEDISTDSAPPLICABLECURRLIABField = value;
            }
        }

        /// <remarks/>
        public object ISPRODUCTCODEEDITED
        {
            get
            {
                return this.iSPRODUCTCODEEDITEDField;
            }
            set
            {
                this.iSPRODUCTCODEEDITEDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SERVICETAXDETAILS.LIST")]
        public object SERVICETAXDETAILSLIST
        {
            get
            {
                return this.sERVICETAXDETAILSLISTField;
            }
            set
            {
                this.sERVICETAXDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LBTREGNDETAILS.LIST")]
        public object LBTREGNDETAILSLIST
        {
            get
            {
                return this.lBTREGNDETAILSLISTField;
            }
            set
            {
                this.lBTREGNDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("VATDETAILS.LIST")]
        public object VATDETAILSLIST
        {
            get
            {
                return this.vATDETAILSLISTField;
            }
            set
            {
                this.vATDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SALESTAXCESSDETAILS.LIST")]
        public object SALESTAXCESSDETAILSLIST
        {
            get
            {
                return this.sALESTAXCESSDETAILSLISTField;
            }
            set
            {
                this.sALESTAXCESSDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("GSTDETAILS.LIST")]
        public object GSTDETAILSLIST
        {
            get
            {
                return this.gSTDETAILSLISTField;
            }
            set
            {
                this.gSTDETAILSLISTField = value;
            }
        }

          

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EXCISETARIFFDETAILS.LIST")]
        public object EXCISETARIFFDETAILSLIST
        {
            get
            {
                return this.eXCISETARIFFDETAILSLISTField;
            }
            set
            {
                this.eXCISETARIFFDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TCSCATEGORYDETAILS.LIST")]
        public object TCSCATEGORYDETAILSLIST
        {
            get
            {
                return this.tCSCATEGORYDETAILSLISTField;
            }
            set
            {
                this.tCSCATEGORYDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TDSCATEGORYDETAILS.LIST")]
        public object TDSCATEGORYDETAILSLIST
        {
            get
            {
                return this.tDSCATEGORYDETAILSLISTField;
            }
            set
            {
                this.tDSCATEGORYDETAILSLISTField = value;
            }
        }

         
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EXCISEJURISDICTIONDETAILS.LIST")]
        public object EXCISEJURISDICTIONDETAILSLIST
        {
            get
            {
                return this.eXCISEJURISDICTIONDETAILSLISTField;
            }
            set
            {
                this.eXCISEJURISDICTIONDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EXCLUDEDTAXATIONS.LIST")]
        public object EXCLUDEDTAXATIONSLIST
        {
            get
            {
                return this.eXCLUDEDTAXATIONSLISTField;
            }
            set
            {
                this.eXCLUDEDTAXATIONSLISTField = value;
            }
        }

       

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CANCELLEDPAYALLOCATIONS.LIST")]
        public object CANCELLEDPAYALLOCATIONSLIST
        {
            get
            {
                return this.cANCELLEDPAYALLOCATIONSLISTField;
            }
            set
            {
                this.cANCELLEDPAYALLOCATIONSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ECHEQUEPRINTLOCATION.LIST")]
        public object ECHEQUEPRINTLOCATIONLIST
        {
            get
            {
                return this.eCHEQUEPRINTLOCATIONLISTField;
            }
            set
            {
                this.eCHEQUEPRINTLOCATIONLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ECHEQUEPAYABLELOCATION.LIST")]
        public object ECHEQUEPAYABLELOCATIONLIST
        {
            get
            {
                return this.eCHEQUEPAYABLELOCATIONLISTField;
            }
            set
            {
                this.eCHEQUEPAYABLELOCATIONLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EDDPRINTLOCATION.LIST")]
        public object EDDPRINTLOCATIONLIST
        {
            get
            {
                return this.eDDPRINTLOCATIONLISTField;
            }
            set
            {
                this.eDDPRINTLOCATIONLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EDDPAYABLELOCATION.LIST")]
        public object EDDPAYABLELOCATIONLIST
        {
            get
            {
                return this.eDDPAYABLELOCATIONLISTField;
            }
            set
            {
                this.eDDPAYABLELOCATIONLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AVAILABLETRANSACTIONTYPES.LIST")]
        public object AVAILABLETRANSACTIONTYPESLIST
        {
            get
            {
                return this.aVAILABLETRANSACTIONTYPESLISTField;
            }
            set
            {
                this.aVAILABLETRANSACTIONTYPESLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LEDPAYINSCONFIGS.LIST")]
        public object LEDPAYINSCONFIGSLIST
        {
            get
            {
                return this.lEDPAYINSCONFIGSLISTField;
            }
            set
            {
                this.lEDPAYINSCONFIGSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TYPECODEDETAILS.LIST")]
        public object TYPECODEDETAILSLIST
        {
            get
            {
                return this.tYPECODEDETAILSLISTField;
            }
            set
            {
                this.tYPECODEDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FIELDVALIDATIONDETAILS.LIST")]
        public object FIELDVALIDATIONDETAILSLIST
        {
            get
            {
                return this.fIELDVALIDATIONDETAILSLISTField;
            }
            set
            {
                this.fIELDVALIDATIONDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("INPUTCRALLOCS.LIST")]
        public object INPUTCRALLOCSLIST
        {
            get
            {
                return this.iNPUTCRALLOCSLISTField;
            }
            set
            {
                this.iNPUTCRALLOCSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("GSTCLASSFNIGSTRATES.LIST")]
        public object GSTCLASSFNIGSTRATESLIST
        {
            get
            {
                return this.gSTCLASSFNIGSTRATESLISTField;
            }
            set
            {
                this.gSTCLASSFNIGSTRATESLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EXTARIFFDUTYHEADDETAILS.LIST")]
        public object EXTARIFFDUTYHEADDETAILSLIST
        {
            get
            {
                return this.eXTARIFFDUTYHEADDETAILSLISTField;
            }
            set
            {
                this.eXTARIFFDUTYHEADDETAILSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("VOUCHERTYPEPRODUCTCODES.LIST")]
        public object VOUCHERTYPEPRODUCTCODESLIST
        {
            get
            {
                return this.vOUCHERTYPEPRODUCTCODESLISTField;
            }
            set
            {
                this.vOUCHERTYPEPRODUCTCODESLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SISCUSTOMERDETSUDF.LIST", Namespace = "TallyUDF")]
        public SISCUSTOMERDETSUDFLIST SISCUSTOMERDETSUDFLIST
        {
            get
            {
                return this.sISCUSTOMERDETSUDFLISTField;
            }
            set
            {
                this.sISCUSTOMERDETSUDFLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("MONTHSRVCAMTUDF.LIST", Namespace = "TallyUDF")]
        public MONTHSRVCAMTUDFLIST MONTHSRVCAMTUDFLIST
        {
            get
            {
                return this.mONTHSRVCAMTUDFLISTField;
            }
            set
            {
                this.mONTHSRVCAMTUDFLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SISSUBSVALIDFROM.LIST", Namespace = "TallyUDF")]
        public SISSUBSVALIDFROMLIST SISSUBSVALIDFROMLIST
        {
            get
            {
                return this.sISSUBSVALIDFROMLISTField;
            }
            set
            {
                this.sISSUBSVALIDFROMLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SISSUBSVALIDUPTO.LIST", Namespace = "TallyUDF")]
        public SISSUBSVALIDUPTOLIST SISSUBSVALIDUPTOLIST
        {
            get
            {
                return this.sISSUBSVALIDUPTOLISTField;
            }
            set
            {
                this.sISSUBSVALIDUPTOLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SISKITTYPEUDF.LIST", Namespace = "TallyUDF")]
        public SISKITTYPEUDFLIST SISKITTYPEUDFLIST
        {
            get
            {
                return this.sISKITTYPEUDFLISTField;
            }
            set
            {
                this.sISKITTYPEUDFLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SISBILLTYPE.LIST", Namespace = "TallyUDF")]
        public SISBILLTYPELIST SISBILLTYPELIST
        {
            get
            {
                return this.sISBILLTYPELISTField;
            }
            set
            {
                this.sISBILLTYPELISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SISSRVCTYPE.LIST", Namespace = "TallyUDF")]
        public SISSRVCTYPELIST SISSRVCTYPELIST
        {
            get
            {
                return this.sISSRVCTYPELISTField;
            }
            set
            {
                this.sISSRVCTYPELISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("MONTHSRVCITEMUDF.LIST", Namespace = "TallyUDF")]
        public MONTHSRVCITEMUDFLIST MONTHSRVCITEMUDFLIST
        {
            get
            {
                return this.mONTHSRVCITEMUDFLISTField;
            }
            set
            {
                this.mONTHSRVCITEMUDFLISTField = value;
            }
        }

         
    }
 

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    [System.Xml.Serialization.XmlRootAttribute("SISCUSTOMERDETSUDF.LIST", Namespace = "TallyUDF", IsNullable = false)]
    public partial class SISCUSTOMERDETSUDFLIST
    {

        private SISCUSTOMERDETSUDFLISTSISCUSTOMERDETSUDF sISCUSTOMERDETSUDFField;

        private string dESCField;

        private string iSLISTField;

        private string tYPEField;

        private ushort iNDEXField;

        /// <remarks/>
        public SISCUSTOMERDETSUDFLISTSISCUSTOMERDETSUDF SISCUSTOMERDETSUDF
        {
            get
            {
                return this.sISCUSTOMERDETSUDFField;
            }
            set
            {
                this.sISCUSTOMERDETSUDFField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ISLIST
        {
            get
            {
                return this.iSLISTField;
            }
            set
            {
                this.iSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort INDEX
        {
            get
            {
                return this.iNDEXField;
            }
            set
            {
                this.iNDEXField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    public partial class SISCUSTOMERDETSUDFLISTSISCUSTOMERDETSUDF
    {

        private string dESCField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    [System.Xml.Serialization.XmlRootAttribute("MONTHSRVCAMTUDF.LIST", Namespace = "TallyUDF", IsNullable = false)]
    public partial class MONTHSRVCAMTUDFLIST
    {

        private MONTHSRVCAMTUDFLISTMONTHSRVCAMTUDF mONTHSRVCAMTUDFField;

        private string dESCField;

        private string iSLISTField;

        private string tYPEField;

        private ushort iNDEXField;

        /// <remarks/>
        public MONTHSRVCAMTUDFLISTMONTHSRVCAMTUDF MONTHSRVCAMTUDF
        {
            get
            {
                return this.mONTHSRVCAMTUDFField;
            }
            set
            {
                this.mONTHSRVCAMTUDFField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ISLIST
        {
            get
            {
                return this.iSLISTField;
            }
            set
            {
                this.iSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort INDEX
        {
            get
            {
                return this.iNDEXField;
            }
            set
            {
                this.iNDEXField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    public partial class MONTHSRVCAMTUDFLISTMONTHSRVCAMTUDF
    {

        private string dESCField;

        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    [System.Xml.Serialization.XmlRootAttribute("SISSUBSVALIDFROM.LIST", Namespace = "TallyUDF", IsNullable = false)]
    public partial class SISSUBSVALIDFROMLIST
    {

        private SISSUBSVALIDFROMLISTSISSUBSVALIDFROM sISSUBSVALIDFROMField;

        private string dESCField;

        private string iSLISTField;

        private string tYPEField;

        private ushort iNDEXField;

        /// <remarks/>
        public SISSUBSVALIDFROMLISTSISSUBSVALIDFROM SISSUBSVALIDFROM
        {
            get
            {
                return this.sISSUBSVALIDFROMField;
            }
            set
            {
                this.sISSUBSVALIDFROMField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ISLIST
        {
            get
            {
                return this.iSLISTField;
            }
            set
            {
                this.iSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort INDEX
        {
            get
            {
                return this.iNDEXField;
            }
            set
            {
                this.iNDEXField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    public partial class SISSUBSVALIDFROMLISTSISSUBSVALIDFROM
    {

        private string dESCField;

        private uint valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public uint Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    [System.Xml.Serialization.XmlRootAttribute("SISSUBSVALIDUPTO.LIST", Namespace = "TallyUDF", IsNullable = false)]
    public partial class SISSUBSVALIDUPTOLIST
    {

        private SISSUBSVALIDUPTOLISTSISSUBSVALIDUPTO sISSUBSVALIDUPTOField;

        private string dESCField;

        private string iSLISTField;

        private string tYPEField;

        private ushort iNDEXField;

        /// <remarks/>
        public SISSUBSVALIDUPTOLISTSISSUBSVALIDUPTO SISSUBSVALIDUPTO
        {
            get
            {
                return this.sISSUBSVALIDUPTOField;
            }
            set
            {
                this.sISSUBSVALIDUPTOField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ISLIST
        {
            get
            {
                return this.iSLISTField;
            }
            set
            {
                this.iSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort INDEX
        {
            get
            {
                return this.iNDEXField;
            }
            set
            {
                this.iNDEXField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    public partial class SISSUBSVALIDUPTOLISTSISSUBSVALIDUPTO
    {

        private string dESCField;

        private uint valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public uint Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    [System.Xml.Serialization.XmlRootAttribute("SISKITTYPEUDF.LIST", Namespace = "TallyUDF", IsNullable = false)]
    public partial class SISKITTYPEUDFLIST
    {

        private SISKITTYPEUDFLISTSISKITTYPEUDF sISKITTYPEUDFField;

        private string dESCField;

        private string iSLISTField;

        private string tYPEField;

        private ushort iNDEXField;

        /// <remarks/>
        public SISKITTYPEUDFLISTSISKITTYPEUDF SISKITTYPEUDF
        {
            get
            {
                return this.sISKITTYPEUDFField;
            }
            set
            {
                this.sISKITTYPEUDFField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ISLIST
        {
            get
            {
                return this.iSLISTField;
            }
            set
            {
                this.iSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort INDEX
        {
            get
            {
                return this.iNDEXField;
            }
            set
            {
                this.iNDEXField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    public partial class SISKITTYPEUDFLISTSISKITTYPEUDF
    {

        private string dESCField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    [System.Xml.Serialization.XmlRootAttribute("SISBILLTYPE.LIST", Namespace = "TallyUDF", IsNullable = false)]
    public partial class SISBILLTYPELIST
    {

        private SISBILLTYPELISTSISBILLTYPE sISBILLTYPEField;

        private string dESCField;

        private string iSLISTField;

        private string tYPEField;

        private ushort iNDEXField;

        /// <remarks/>
        public SISBILLTYPELISTSISBILLTYPE SISBILLTYPE
        {
            get
            {
                return this.sISBILLTYPEField;
            }
            set
            {
                this.sISBILLTYPEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ISLIST
        {
            get
            {
                return this.iSLISTField;
            }
            set
            {
                this.iSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort INDEX
        {
            get
            {
                return this.iNDEXField;
            }
            set
            {
                this.iNDEXField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    public partial class SISBILLTYPELISTSISBILLTYPE
    {

        private string dESCField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    [System.Xml.Serialization.XmlRootAttribute("SISSRVCTYPE.LIST", Namespace = "TallyUDF", IsNullable = false)]
    public partial class SISSRVCTYPELIST
    {

        private SISSRVCTYPELISTSISSRVCTYPE sISSRVCTYPEField;

        private string dESCField;

        private string iSLISTField;

        private string tYPEField;

        private ushort iNDEXField;

        /// <remarks/>
        public SISSRVCTYPELISTSISSRVCTYPE SISSRVCTYPE
        {
            get
            {
                return this.sISSRVCTYPEField;
            }
            set
            {
                this.sISSRVCTYPEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ISLIST
        {
            get
            {
                return this.iSLISTField;
            }
            set
            {
                this.iSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort INDEX
        {
            get
            {
                return this.iNDEXField;
            }
            set
            {
                this.iNDEXField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    public partial class SISSRVCTYPELISTSISSRVCTYPE
    {

        private string dESCField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    [System.Xml.Serialization.XmlRootAttribute("MONTHSRVCITEMUDF.LIST", Namespace = "TallyUDF", IsNullable = false)]
    public partial class MONTHSRVCITEMUDFLIST
    {

        private MONTHSRVCITEMUDFLISTMONTHSRVCITEMUDF mONTHSRVCITEMUDFField;

        private string dESCField;

        private string iSLISTField;

        private string tYPEField;

        private ushort iNDEXField;

        /// <remarks/>
        public MONTHSRVCITEMUDFLISTMONTHSRVCITEMUDF MONTHSRVCITEMUDF
        {
            get
            {
                return this.mONTHSRVCITEMUDFField;
            }
            set
            {
                this.mONTHSRVCITEMUDFField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ISLIST
        {
            get
            {
                return this.iSLISTField;
            }
            set
            {
                this.iSLISTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort INDEX
        {
            get
            {
                return this.iNDEXField;
            }
            set
            {
                this.iNDEXField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "TallyUDF")]
    public partial class MONTHSRVCITEMUDFLISTMONTHSRVCITEMUDF
    {

        private string dESCField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESC
        {
            get
            {
                return this.dESCField;
            }
            set
            {
                this.dESCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }


}


